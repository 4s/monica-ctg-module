#include <iostream>
#include <thread>
#include <chrono>
#include <gtest/gtest.h>
#include <nlohmann/json.hpp>
#include "monica-ctg.hpp"
#include "Monica.pb.h"
#include "DeviceControl.pb.h"
#include "SerialPort.pb.h"
#include "FHIRMeasurement.pb.h"

using namespace s4::monica_ctg;
using namespace s4::BasePlate;
using namespace s4::messages;
using nlohmann::json;
using std::unique_ptr;
using std::shared_ptr;
using std::set;
using std::vector;


#define SERIALPORT_HANDLE 42


/**********************************************************************/
/*                                                                    */
/*                          Helper functions                          */
/*                                                                    */
/**********************************************************************/


/// Wait for about 1 second for the predicate to become true. 
bool await(std::function<bool()> predicate) {
  int current_iteration = 1;
  int max_iterations = 100;
  int milliseconds_sleep = 10;

  do {
    std::this_thread::sleep_for(std::chrono::milliseconds(milliseconds_sleep));
    if (current_iteration++ >= max_iterations) {
      return false;
    }
  }
  while(!predicate());

  return true;
}


/// True if s2 is contained in s1
bool contains(string s1, string s2) {
  return s1.find(s2) != std::string::npos;
}




/**********************************************************************/
/*                                                                    */
/*                            Stub modules                            */
/*                                                                    */
/**********************************************************************/


///
/// Time module stub
///
class TimeStub : public ModuleBase {

public:
  
  TimeStub(Context& context) : ModuleBase(context, "00000001") {
    addFunction<interfaces::time::C2P>("Time.C2P", [this](MetaData metadata, interfaces::time::C2P* msg) -> void {
        if (msg->has_event()) {
          interfaces::time::C2P::Event event = msg->event();
          if (event.has_solicit_clock()) {
            announceTimeTo(metadata.sender);
          }
        } else if (msg->has_request()) {
          interfaces::time::C2P::Request request = msg->request();
          if (request.has_start_timer()) {
            // We currently do not support repeating timers...
            setTimer(metadata.sender, ++id, false);
            if (allowTimer) {
              int delay = request.start_timer().delay_millis();
              if (millis >= 0) {
                delay = millis;
              }
              std::this_thread::sleep_for(std::chrono::milliseconds(delay));
              setTimer(metadata.sender, id, true);
            }
          }
        }
      });
    start();
  }

  /// Set to true if the timer is used by the test. Note that the
  /// timer is implemented in a way that requires it to expire before
  /// the test can complete. The module will not shut down before the
  /// timer expires.
  void setAllowTimer(bool value) { allowTimer = value; }

  void overrideTimerDelay(int millis) {
    this->millis = millis;
  }
  
private:

  volatile bool allowTimer = false;
  int id = 1;
  int millis = -1;
  
  void announceTimeTo(peer_t peer) {
    interfaces::time::AnnounceClock* announce_clock = new interfaces::time::AnnounceClock();

    interfaces::time::P2C::Event* event = new interfaces::time::P2C::Event();
    event->set_allocated_announce_clock(announce_clock);

    interfaces::time::P2C* msg = new interfaces::time::P2C();
    msg->set_allocated_event(event);

    sendUnicast(peer, "Time.P2C", "N/A", msg);
  }

  void setTimer(peer_t peer, int id, bool fired) {
    interfaces::time::StartTimerSuccess* start_timer_success = new interfaces::time::StartTimerSuccess();
    start_timer_success->set_timer_id(id);
    start_timer_success->set_fired(fired);

    sendUnicast(peer, "#0003", "OK.LAST", start_timer_success);
  }  
};




///
/// DeviceControl module stub
///
class DeviceControlStub : public ModuleBase {

public:

  DeviceControlStub(Context& context) : ModuleBase(context, "00000002") {
    addFunction<interfaces::device_control::C2P>("DeviceControl.C2P", [this](MetaData metadata, interfaces::device_control::C2P* msg) -> void {
        if (msg->has_event()) {
          interfaces::device_control::C2P::Event event = msg -> event();
          if (event.has_search()) {
            
            // Announce a fake device
            interfaces::device_control::Search search = event.search();
            searchInitiated(search.search_id());
            if (willAnnounceDevice) {
              announceDevice(search.search_id());
            }
          }
        }
      });
    start();
  }

  void setAnnounceDevice(bool value) { willAnnounceDevice = value; }
  
private:

  volatile bool willAnnounceDevice = true;
  
  void searchInitiated(objId_t search_id) {
    interfaces::device_control::SearchInitiated* search_initiated = new interfaces::device_control::SearchInitiated();
    search_initiated->set_search_id(search_id);

    interfaces::device_control::P2C::Event* event = new interfaces::device_control::P2C::Event();
    event->set_allocated_search_initiated(search_initiated);

    interfaces::device_control::P2C* msg = new interfaces::device_control::P2C();
    msg->set_allocated_event(event);

    sendMulticast("DeviceControl.P2C", "N/A", msg);
  }

  void announceDevice(objId_t search_id) {
    classes::device::BluetoothDevice* bluetooth_device = new classes::device::BluetoothDevice();
    bluetooth_device->set_bd_addr("00:80:98:01:02:03");
    bluetooth_device->set_name("AN24 A123456");

    classes::device::BluetoothDevice_BluetoothProfile* bluetooth_device_profile = bluetooth_device->add_bluetooth_profiles();
    bluetooth_device_profile->set_profile(classes::device::BluetoothDevice_BluetoothProfile_Profile_SERIAL_PORT_PROFILE);
    // The serial port is handled by the module with id = 3 (the SerialPortStub)
    bluetooth_device_profile->set_handler(3);
    bluetooth_device_profile->set_handle(SERIALPORT_HANDLE);

    classes::device::DeviceTransport* device_transport = new classes::device::DeviceTransport();
    device_transport->set_allocated_bluetooth_device(bluetooth_device);

    interfaces::device_control::TransportBundle* transport_bundle = new interfaces::device_control::TransportBundle();
    transport_bundle->set_allocated_device_transport(device_transport);

    interfaces::device_control::SearchResult* search_result = new interfaces::device_control::SearchResult();
    search_result->set_allocated_result(transport_bundle);
    search_result->set_search_id(search_id);

    interfaces::device_control::P2C::Event* event = new interfaces::device_control::P2C::Event();
    event->set_allocated_search_result(search_result);

    interfaces::device_control::P2C* msg = new interfaces::device_control::P2C();
    msg->set_allocated_event(event);

    sendMulticast("DeviceControl.P2C", "N/A", msg);
  }
};




///
/// SerialPort module stub (the Monica AN24 device emulator)
///
class SerialPortStub : public ModuleBase {

public:

  SerialPortStub(Context& context) : ModuleBase(context, "00000003"), peer("") {
    addFunction<interfaces::serial_port::C2P> ("SerialPort.C2P", [this](MetaData metadata,
                                                                        interfaces::serial_port::C2P* msg) -> void {
        if (msg->has_request()) {
          interfaces::serial_port::C2P_Request request = msg->request();
          if (request.has_open()) {
            // SerialPort::Open
            interfaces::serial_port::Open open = request.open();
            if (open.serial_port_handle() == SERIALPORT_HANDLE) {
              if (!peer.empty()) {
                sendOpenFailed(metadata.sender,2); // Port already open
              } else if (noConnection) {
                sendOpenFailed(metadata.sender,5); // Connection failed
              } else {
                peer = metadata.sender;
                sendOpenSuccess(false);
              }
            } else {
              sendOpenFailed(metadata.sender,1); // Unknown port
            }
          } else if (request.has_write()) {
            // SerialPort::Write
            interfaces::serial_port::Write write = request.write();
            if (write.serial_port_handle() == SERIALPORT_HANDLE) {
              if (peer != metadata.sender) {
                sendWriteFailed(metadata.sender,3); // Not open
              } else {
                handleWrite(write.message());
              }
            } else {
              sendWriteFailed(metadata.sender,1); // Unknown port
            }
          } else {
            std::cout << "Unsupported serial port request, open and write is expected\n";
          }
        }
        else if (msg->has_event()) {
          interfaces::serial_port::C2P_Event event = msg->event();
          if (event.has_close()) {
            // SerialPort::Close
            interfaces::serial_port::Close close = event.close();
            if (close.serial_port_handle() == SERIALPORT_HANDLE) {
              if (peer == metadata.sender) {
                sendClosed();
              } // else ignore
            }
          } else {
            std::cout << "Unsupported serial port event\n";
          }
        }
        else {
          std::cout << "Serial port message without request or event\n";
        }
      });
    start();
  }

  void setNoConnection(bool value) { noConnection = value; }
  void setLeadCheckValues(vector<uint16_t> value) { leadCheckValues = value; }
  
  void sendMeasurement(uint16_t *fhr, uint16_t *mhr, uint8_t *toco, uint8_t *qfhr) {
    unsigned char m[] = "C\000\0000000000000000000000000000000A`\010";
    for (int i=0; i<4; i++) {
      uint16_t x = fhr[i] & 0x07FF;
      m[i*2+3] = x >> 8;
      m[i*2+4] = x & 0x00ff;
      x = (mhr[i] & 0x07ff) | ((qfhr[i] & 0x03) << 13);
      m[i*2+19] = x >> 8;
      m[i*2+20] = x & 0x00ff;
      m[i+27] = toco[i];
    }
    sendReadMessage(string(m, m + sizeof m - 1)); // String contains NUL characters
  }

  void sendDummyMeasurement() {
    uint16_t fhr[] {0,0,0,0};
    uint16_t mhr[] {0,0,0,0};
    uint8_t toco[] {0,0,0,0};
    uint8_t qfhr[] {0,0,0,0};
    sendMeasurement(fhr, mhr, toco, qfhr);
  }
  
  void sendMarkerEvent() {
    sendReadMessage("MM");
  }

  void sendEnd() {
    sendReadMessage("N02ANEND000000");
  }
  
  void sendClosed() {

    if (!peer.empty()) {
      sendOpenSuccess(true);
    }
    peer = "";

    interfaces::serial_port::Closed* closed = new interfaces::serial_port::Closed();
    closed->set_serial_port_handle(SERIALPORT_HANDLE);
    
    interfaces::serial_port::P2C::Event* event = new interfaces::serial_port::P2C::Event();
    event->set_allocated_closed(closed);

    interfaces::serial_port::P2C* msg = new interfaces::serial_port::P2C();
    msg->set_allocated_event(event);
    sendMulticast("SerialPort.P2C", "N/A", msg);
  }

private:

  volatile bool noConnection = false;
  vector<uint16_t> leadCheckValues {0xf100};
  peer_t peer;
  
  void sendOpenSuccess(bool last) {
    interfaces::serial_port::OpenSuccess* msg = new interfaces::serial_port::OpenSuccess();
    if (last) {
      sendUnicast(peer, "#0001", "OK.LAST", msg);
    } else {
      sendUnicast(peer, "#0001", "OK.MORE", msg);
    }
  }

  void sendOpenFailed(peer_t peer, int errorcode) {
    classes::error::Error* msg = new classes::error::Error();
    msg->set_interface_name("serial_port");
    msg->set_error_code(errorcode);
    sendUnicast(peer, "#0001", "ERR.LAST", msg);
  }

  void sendWriteSuccess() {
    interfaces::serial_port::WriteSuccess* msg = new interfaces::serial_port::WriteSuccess();
    sendUnicast(peer, "#0002", "OK.LAST", msg);
  }

  void sendWriteFailed(peer_t peer, int errorcode) {
    classes::error::Error* msg = new classes::error::Error();
    msg->set_interface_name("serial_port");
    msg->set_error_code(errorcode);
    sendUnicast(peer, "#0002", "ERR.LAST", msg);
  }

  void handleWrite(string input) {
    if (isOfdCommand(input) || isContCommand(input) || isGCommand(input) || isHaltCommand(input) || isEndCommand(input)) {
      sendWriteSuccess();
    } else if (isVersionCommand(input)) {
      sendWriteSuccessAndResponse("IdeviId123SWREVNR0000004567");
    } else if (isPstCommand(input)) {
      sendWriteSuccessAndResponse("N02ANP00000000");
    } else if (isUADCommand(input)) {
      sendWriteSuccessAndResponse("N02ANU00000000");
    } else if (isIM3Command(input)) {
      string msg = "N02ANiXY";
      uint16_t currentValue = leadCheckValues.back();
      leadCheckValues.pop_back();
      msg[6] = currentValue >> 8;
      msg[7] = currentValue & 0x00FF;
      sendWriteSuccessAndResponse(msg);
    }
    else {
      std::cout << "Got unknown command: " << input << "\n";
    }
  }
  
  void sendWriteSuccessAndResponse(string message) {
    sendWriteSuccess();

    // Wait for the initial OK to sink in and then send another message
    std::this_thread::sleep_for(std::chrono::milliseconds(20));

    // Send the response
    sendReadMessage(message);
  }

  void crcCalc(unsigned char x, uint16_t &crc) {
    crc ^= (((uint16_t)x) << 8);
    int i = 8;
    do {
      if (crc & 0x8000)
        crc = crc << 1 ^ 0x1021;
      else
        crc = crc << 1;
    } while(--i);
  }

  void sendReadMessage(string message) {
    vector<unsigned char> msg_v = vector<unsigned char>(message.begin(), message.end());
    vector<unsigned char> crcMessage = createMsg(msg_v);
    interfaces::serial_port::OpenSuccess* openSuccess = new interfaces::serial_port::OpenSuccess();
    openSuccess->set_message(string(crcMessage.begin(), crcMessage.end()));
    sendUnicast(peer, "#0001", "OK.MORE", openSuccess);
  }

  vector<unsigned char> createMsg(vector<unsigned char> &msg) {
    vector<unsigned char> output;
    uint16_t crc = 0;
    output.reserve(msg.size() + 6);
    output.push_back(0x10);
    crcCalc(0x10,crc);
    output.push_back(0x02);
    crcCalc(0x02,crc);
    for (unsigned char c : msg) {
      crcCalc(c,crc);
      output.push_back(c);
      if (c == 0x10) {
        crcCalc(c,crc);
        output.push_back(c);
      }
    }
    output.push_back(0x10);
    crcCalc(0x10,crc);
    output.push_back(0x03);
    crcCalc(0x03,crc);
    output.push_back((crc >> 8) & 0xFF);
    output.push_back(crc & 0xFF);

    return output;
  }

  bool isVersionCommand(string input) {
    return input[2] == '?' && input[3] == 'I';
  }

  bool isPstCommand(string input) {
    return input[7] == 'P' && input[8] == 'S' && input[9] == 'T';
  }

  bool isUADCommand(string input) {
    return input[7] == 'U' && input[8] == 'A' && input[9] == 'D';
  }

  bool isIM3Command(string input) {
    return input[7] == 'I' && input[8] == 'M' && input[9] == '3';
  }

  bool isOfdCommand(string input) {
    return input[7] == 'O' && input[8] == 'F' && input[9] == 'D';
  }

  bool isContCommand(string input) {
    return input[7] == 'C' && input[8] == 'O' && input[9] == 'N' && input[10] == 'T';
  }

  bool isEndCommand(string input) {
    return input[7] == 'E' && input[8] == 'N' && input[9] == 'D';
  }

  bool isHaltCommand(string input) {
    return input[2] == 'H';
  }

  bool isGCommand(string input) {
    return input[2] == 'G';
  }
};




///
/// Monica module stub (the application's Monica control API)
///
class MonicaStub : public ModuleBase {

public:

  MonicaStub(Context& context) : ModuleBase(context, "00000004") {
    addFunction<interfaces::monica::P2C>("Monica.P2C", [this](MetaData metadata, interfaces::monica::P2C* msg) -> void {
        if (msg->has_event()) {
          interfaces::monica::P2C_Event event = msg->event();
          if (event.has_current_status()) {
            interfaces::monica::CurrentStatus a = event.current_status();
//            std::cout << "Status: " << a.current_status() << "\n";
            statusList.push_back(a.current_status());
          }
        }
      });
    start();
  }

  void sendStart() {
    interfaces::monica::Start* start = new interfaces::monica::Start();

    interfaces::monica::C2P::Event* event = new interfaces::monica::C2P::Event();
    event->set_allocated_start(start);

    interfaces::monica::C2P* msg = new interfaces::monica::C2P();
    msg->set_allocated_event(event);

    sendMulticast("Monica.C2P", "N/A", msg);
  }

  void sendStop() {
    interfaces::monica::Stop* stop = new interfaces::monica::Stop();

    interfaces::monica::C2P::Event* event = new interfaces::monica::C2P::Event();
    event->set_allocated_stop(stop);

    interfaces::monica::C2P* msg = new interfaces::monica::C2P();
    msg->set_allocated_event(event);

    sendMulticast("Monica.C2P", "N/A", msg);
  }

  bool awaitStatus(string status) {
    return await([&]{return contains(statusList.back(),status);});
  }

  bool awaitStatus(string status, int seconds) {
    for (int i=0; i<seconds; i++) { 
      if (awaitStatus(status)) return true;
    }
    return false;
  }

  /// List of the events (in order) reported during the test on the Monica interface
  vector<string> getStatusList() {
    return statusList;
  }

private:

  /// List of the events (in order) reported during the test on the Monica interface
  vector<string> statusList;
};



    
/// Wrapper of the CTG module FHIRMeasurement output
struct Measurement {
  string type, device, observation;
};


  
///
/// FHIRMeasurement module stub
///
class FHIRMeasurementStub : public ModuleBase {

public:
  
  FHIRMeasurementStub(Context& context) : ModuleBase(context, "00000042"), measurement(new Measurement()) {

    addFunction<interfaces::fhir_measurement::P2C>("FHIRMeasurement.P2C", [this](MetaData metadata, interfaces::fhir_measurement::P2C* msg) -> void {
        if(msg->has_event()) {
          interfaces::fhir_measurement::P2C::Event event = msg->event();
          if (event.has_measurement_received()) {
            interfaces::fhir_measurement::MeasurementReceived measurement_received = event.measurement_received();
            measurement->type        = measurement_received.device_type();
            measurement->device      = measurement_received.fhir_device();
            measurement->observation = measurement_received.fhir_observation();
          } else {
            std::cout << "Fhir measurement message without data\n";
          }
        } else {
          std::cout << "Fhir measurement without event\n";
        }
      });
    start();
  }

  /// The resulting measurement as reported to the FHIRMeasurement interface at the end of the test
  shared_ptr<Measurement> getMeasurement() {
    return measurement;
  }

private:
  
  /// The resulting measurement as reported to the FHIRMeasurement interface at the end of the test
  shared_ptr<Measurement> measurement;
};







/**********************************************************************/
/*                                                                    */
/*                          Integration tests                         */
/*                                                                    */
/**********************************************************************/


//
// The common test environment - will be (re-)initialized for each test.
//

Context* context;
Reflector* reflector;
MasterModule* master;
ModuleBase* monica;
TimeStub *timeStub;
DeviceControlStub *dcStub;
SerialPortStub *serialStub;
MonicaStub *monicaStub;
FHIRMeasurementStub* measurementStub;


/// Initialize the test environment for a new test
#define TEST_HEADER \
  context = new Context(); \
  reflector = new Reflector(context); \
  master = new MasterModule(*context, 7); \
  monica = new MonicaCTG(*context); \
  timeStub = new TimeStub(*context); \
  dcStub = new DeviceControlStub(*context); \
  serialStub = new SerialPortStub(*context); \
  monicaStub = new MonicaStub(*context); \
  measurementStub = new FHIRMeasurementStub(*context); \
  \
  reflector->start(); \
  \
  if (!await([&]{return monica->getApplicationState() == RUNNING;})) { \
    master->requestShutdown(true); \
    FAIL() << "Monica module did not go into running state"; \
  }

/// Finalize the test, await module shutdown, and clear the environment
#define TEST_FOOTER \
  master->requestShutdown(); \
  \
  if (!await([&]{return monica->getApplicationState() == FINALIZING;})) { \
    master->requestShutdown(true); \
    FAIL() << "Monica module did not shutdown"; \
  } \
  \
  delete measurementStub; measurementStub = nullptr; \
  delete monicaStub; monicaStub = nullptr; \
  delete serialStub; serialStub = nullptr; \
  delete dcStub; dcStub = nullptr; \
  delete timeStub; timeStub = nullptr; \
  delete monica; monica = nullptr; \
  delete master; master = nullptr; \
  delete reflector; reflector = nullptr; \
  delete context; context = nullptr; \


/// Validate the measurement result, including a few basic (fixed) elements of the Observation and Device resources 
void validateMeasurement(Measurement& measurement, json& device, json& observation) {
  // Test observation is set
  ASSERT_TRUE(measurement.type.length() > 0)                << "No measurement was received";
  ASSERT_TRUE(measurement.device.length() > 0)              << "No measurement was received";
  ASSERT_TRUE(measurement.observation.length() > 0)         << "No measurement was received";

  // Type is a fixed string
  ASSERT_EQ(measurement.type, "MDC585728")                  << "Invalid measurement type";
  try {
    device = json::parse(measurement.device);
  } catch (...) {
    FAIL()                                                  << "Device resource could not be parsed as JSON";
  }
  try {
    observation = json::parse(measurement.observation);
  } catch (...) {
    FAIL()                                                  << "Observation resource could not be parsed as JSON";
  }
  
  // Test fhir objects are valid JSON
  ASSERT_FALSE(device.empty())                              << "Invalid Device resource received";
  ASSERT_FALSE(observation.empty())                         << "Invalid Observation resource received";

  // Test fixed contents of Device resource
  ASSERT_EQ(device["resourceType"], "Device")               << "Device resource does not contain a Device";     

  // Test fixed contents of Observation resource
  ASSERT_EQ(observation["resourceType"], "Observation")     << "Observation resource does not contain an Observation";
}

#define ASSERT_DO_ENTER_STATUS_BEFORE_TIMEOUT(status) ASSERT_TRUE(awaitStatus(status))
#define ASSERT_DO_NOT_ENTER_STATUS_BEFORE_TIMEOUT(status) ASSERT_FALSE(awaitStatus(status))

bool awaitStatus(string status) {
  if (monicaStub->awaitStatus(status)) {
    return true;
  } else {
    master->requestShutdown(true);
    return false;
  }
}




/// Test that the Monica module will start up and shut down
TEST(MonicaCtgModule, CanStartupAndShutdown) {

  TEST_HEADER;

  TEST_FOOTER;
  
  // At this point the modules are stopped, and we know they have been running
  // so the test is a success without doing any assertions.
}



/// Test that a start attempt without a monica device found will fail
TEST(MonicaCtgModule, StartWithoutDeviceFoundCancelFast) {

  TEST_HEADER;

  dcStub->setAnnounceDevice(false);
  timeStub->setAllowTimer(true);
  timeStub->overrideTimerDelay(200);
  monicaStub->sendStart();

  ASSERT_DO_ENTER_STATUS_BEFORE_TIMEOUT("CONNECTING")       << "Monica module did not try to connect";

  ASSERT_DO_ENTER_STATUS_BEFORE_TIMEOUT("DISCONNECTED")     << "Monica module did not disconnect";

  TEST_FOOTER;
}

TEST(MonicaCtgModule, StartWithoutDeviceFoundTimerDisabled) {

  TEST_HEADER;

  dcStub->setAnnounceDevice(false);
  timeStub->setAllowTimer(false);
  monicaStub->sendStart();

  ASSERT_DO_ENTER_STATUS_BEFORE_TIMEOUT("CONNECTING")       << "Monica module did not try to connect";

  ASSERT_DO_NOT_ENTER_STATUS_BEFORE_TIMEOUT("DISCONNECTED") << "Monica module did disconnect, but the cancel timer should not have been triggered yet";

  ASSERT_DO_ENTER_STATUS_BEFORE_TIMEOUT("CONNECTING")       << "Monica module is expected to still be in connecting state";

  TEST_FOOTER;
}


/// Test that a recording can be stopped by sudden disconnect
TEST(MonicaCtgModule, StopByDisconnect) {

  TEST_HEADER;

  monicaStub->sendStart();

  ASSERT_DO_ENTER_STATUS_BEFORE_TIMEOUT("recordingStartedEvent") << "Monica module did not start recording";

  serialStub->sendClosed();
  
  ASSERT_DO_ENTER_STATUS_BEFORE_TIMEOUT("DISCONNECTED")     << "Monica module did not disconnect";
  
  TEST_FOOTER;  
}



/// Test that a recording can be stopped by Monica turning off
TEST(MonicaCtgModule, StopByMonicaOff) {

  TEST_HEADER;

  monicaStub->sendStart();

  ASSERT_DO_ENTER_STATUS_BEFORE_TIMEOUT("recordingStartedEvent") << "Monica module did not start recording";

  serialStub->sendEnd();
  
  ASSERT_DO_ENTER_STATUS_BEFORE_TIMEOUT("DISCONNECTED")     << "Monica module did not disconnect";
  
  TEST_FOOTER;
}



/// Test that a recording can be stopped by "stop" request
TEST(MonicaCtgModule, StopByStopCommand) {

  TEST_HEADER;

  monicaStub->sendStart();

  ASSERT_DO_ENTER_STATUS_BEFORE_TIMEOUT("recordingStartedEvent") << "Monica module did not start recording";

  monicaStub->sendStop();
  
  ASSERT_DO_ENTER_STATUS_BEFORE_TIMEOUT("DISCONNECTED")     << "Monica module did not disconnect";
  
  TEST_FOOTER;
}



/// Test that a start attempt with an inaccessible monica device will fail
TEST(MonicaCtgModule, StartWithoutAccessibleDevice) {

  TEST_HEADER;

  serialStub->setNoConnection(true);
  monicaStub->sendStart();

  ASSERT_DO_ENTER_STATUS_BEFORE_TIMEOUT("DISCONNECTED")     << "Monica module did not disconnect";
  
  TEST_FOOTER;
  
}


/// Test that a simple one-second recording can be performed
TEST(MonicaCtgModule, RecordingPerformed) {

  TEST_HEADER;

  monicaStub->sendStart();

  ASSERT_DO_ENTER_STATUS_BEFORE_TIMEOUT("recordingStartedEvent") << "Monica module did not start recording";

  serialStub->sendDummyMeasurement();

  ASSERT_DO_ENTER_STATUS_BEFORE_TIMEOUT("qualityObservation") << "Measurement was not received";

  monicaStub->sendStop();
  
  ASSERT_DO_ENTER_STATUS_BEFORE_TIMEOUT("DISCONNECTED")     << "Monica module did not disconnect";

  vector<string> sl = monicaStub->getStatusList();
  
  TEST_FOOTER;
  
    // Test list of status events
  ASSERT_EQ(sl.size(), 10)                                  << "Unexpected number of status events received";
  ASSERT_TRUE(contains(sl[0], "CONNECTING"));
  ASSERT_TRUE(contains(sl[1], "CONNECTED"));
  ASSERT_TRUE(contains(sl[2], "leadcheckStartedEvent"));
  ASSERT_TRUE(contains(sl[3], "leadcheckObservation"));
  ASSERT_TRUE(contains(sl[4], "leadcheckStoppedEvent"));
  ASSERT_TRUE(contains(sl[5], "recordingStartedEvent"));
  ASSERT_TRUE(contains(sl[6], "qualityObservation"));
  ASSERT_TRUE(contains(sl[7], "recordingStoppedEvent"));
  ASSERT_TRUE(contains(sl[8], "DISCONNECTING"));
  ASSERT_TRUE(contains(sl[9], "DISCONNECTED"));
}


/// Test Impedance check
TEST(MonicaCtgModule, ImpedanceTesting) {

  TEST_HEADER;

  vector<uint16_t> leadCheckValues {
    0xf100,  // All OK
    0x0100, // Green NG
    0x0101, // Green BAD
    0x0200, // White NG
    0x0202, // White BAD
    0x0400, // Orange NG
    0x0404, // Orange BAD
    0x0800, // Yellow NG
    0x0808, // Yellow BAD
    0x1000, // Black NG
    0x1010 // Black BAD
  };

  serialStub->setLeadCheckValues(leadCheckValues);
  monicaStub->sendStart();

  // Wait for up to 6 seconds...
  if (!monicaStub->awaitStatus("recordingStartedEvent", 6)) {
    master->requestShutdown(true);
    FAIL()                                                  << "Monica module did not start recording";
  }
  
  monicaStub->sendStop();

  ASSERT_DO_ENTER_STATUS_BEFORE_TIMEOUT("DISCONNECTED")     << "Monica module did not disconnect";

  vector<string> sl = monicaStub->getStatusList();

  TEST_FOOTER;

  // Test list of status events
  ASSERT_EQ(sl.size(), 19)                                  << "Unexpected number of status events received";
  // sl[0]:  "CONNECTING"
  // sl[1]:  "CONNECTED"
  ASSERT_TRUE(contains(sl[2], "leadcheckStartedEvent"));
  ASSERT_TRUE(contains(sl[3], "black\":\"BAD"));
  ASSERT_TRUE(contains(sl[4], "black\":\"NG"));
  ASSERT_TRUE(contains(sl[5], "yellow\":\"BAD"));
  ASSERT_TRUE(contains(sl[6], "yellow\":\"NG"));
  ASSERT_TRUE(contains(sl[7], "orange\":\"BAD"));
  ASSERT_TRUE(contains(sl[8], "orange\":\"NG"));
  ASSERT_TRUE(contains(sl[9], "white\":\"BAD"));
  ASSERT_TRUE(contains(sl[10], "white\":\"NG"));
  ASSERT_TRUE(contains(sl[11], "green\":\"BAD"));
  ASSERT_TRUE(contains(sl[12], "green\":\"NG"));
  ASSERT_TRUE(contains(sl[13], "{\"leadcheckObservation\":{\"green\":\"OK\",\"white\":\"OK\",\"orange\":\"OK\",\"yellow\":\"OK\",\"black\":\"OK\"}}"));
  ASSERT_TRUE(contains(sl[14], "leadcheckStoppedEvent"));
  // sl[15]:  "recordingStartedEvent"
  // sl[16]:  "recordingStoppedEvent"
  // sl[17]:  "DISCONNECTING"
  // sl[18]:  "DISCONNECTED"
}


/// Test Device resource contents
TEST(MonicaCtgModule, DeviceResourceContents) {

  TEST_HEADER;

  monicaStub->sendStart();

  ASSERT_DO_ENTER_STATUS_BEFORE_TIMEOUT("recordingStartedEvent") << "Monica module did not start recording";

  serialStub->sendDummyMeasurement();

  ASSERT_DO_ENTER_STATUS_BEFORE_TIMEOUT("qualityObservation") << "Measurement was not received";

  monicaStub->sendStop();
  
  ASSERT_DO_ENTER_STATUS_BEFORE_TIMEOUT("DISCONNECTED")     << "Monica module did not disconnect";

  shared_ptr<Measurement> measurement = measurementStub->getMeasurement();
  
  TEST_FOOTER;
  
  // Test the measurement results
  json dev, obs;
  validateMeasurement(*measurement, dev, obs);

  ASSERT_EQ(dev["id"], "008098FEFF010203.008098010203")     << "Bluetooth address not mapped correctly to FHIR id";

  for (int i = 0; i<dev["identifier"].size(); i++) {
    if (dev["identifier"][i]["system"] == "urn:oid:1.2.840.10004.1.1.1.0.0.1.0.0.1.2680") {
      ASSERT_EQ(dev["identifier"][i]["value"], "00-80-98-FE-FF-01-02-03") << "Bluetooth address not mapped correctly to FHIR EUI-64 identifier";
    } else if (dev["identifier"][i]["system"] == "http://hl7.org/fhir/sid/eui-48") {
      ASSERT_EQ(dev["identifier"][i]["value"], "00-80-98-01-02-03")  << "Bluetooth address not copied correctly to FHIR identifier";
    }
  }

  ASSERT_EQ(dev["manufacturer"], "Monica Healthcare")       << "Invalid device manufacturer";
  ASSERT_EQ(dev["modelNumber"], "deviId")                   << "FHIR Monica device modelNumber wrong";
  ASSERT_EQ(dev["serialNumber"], "A123456")                 << "FHIR Monica device serialNumber wrong";
  
  for (int i = 0; i<dev["version"].size(); i++) {
    if (dev["version"][i]["type"]["coding"][0]["code"] == "531975") {
      ASSERT_EQ(dev["version"][i]["value"], "SWREVNR")      << "FHIR Monica device SW revision wrong";
    } else if (dev["version"][i]["type"]["coding"][0]["code"] == "531976") {
      ASSERT_EQ(dev["version"][i]["value"], "4.567")        << "FHIR Monica device FW revision wrong";
    } else if (dev["version"][i]["type"]["coding"][0]["code"] == "531977") {
      ASSERT_EQ(dev["version"][i]["value"], "1.02.03")      << "FHIR Monica device protocol revision wrong";
    } else if (dev["version"][i]["type"]["coding"][0]["code"] == "532352") {
      ASSERT_EQ(dev["version"][i]["value"], "07.00")        << "FHIR Monica device Continua version wrong";
    }
  }
  
}

string timeFormat(tm &time, const char *format) {
  char timestampBuffer[50];
  size_t len = strftime(timestampBuffer,50,format,&time);
  return string(timestampBuffer, len);
}

/// Test Observation resource contents
TEST(MonicaCtgModule, ObservationResourceContents) {

  TEST_HEADER;

  monicaStub->sendStart();
      
  ASSERT_DO_ENTER_STATUS_BEFORE_TIMEOUT("recordingStartedEvent") << "Monica module did not start recording";

  uint16_t fhr[] {0x0000,0x0001,0x0002,0x0003};
  uint16_t mhr[] {0x0000,0x0001,0x0002,0x0003};
  uint8_t toco[] {0x00,0x01,0x02,0x03};
  uint8_t qfhr[] {0x00,0x01,0x02,0x00};
  
  serialStub->sendMeasurement(fhr, mhr, toco, qfhr);

  fhr[0] = 0x0220; fhr[1] = 0x0221; fhr[2] = 0x0222; fhr[3] = 0x0223; 
  mhr[0] = 0x0101; mhr[1] = 0x0101; mhr[2] = 0x0101; mhr[3] = 0x0101; 
  toco[0] = 0x01;  toco[1] = 0x01;  toco[2] = 0x01;  toco[3] = 0x01; 
  qfhr[0] = 0x01;  qfhr[1] = 0x01;  qfhr[2] = 0x01;  qfhr[3] = 0x01; 

  serialStub->sendMeasurement(fhr, mhr, toco, qfhr);
  serialStub->sendMarkerEvent();

  fhr[0] = 0x0101; fhr[1] = 0x0101; fhr[2] = 0x0101; fhr[3] = 0x0101; 
  mhr[0] = 0x0102; mhr[1] = 0x0103; mhr[2] = 0x0104; mhr[3] = 0x0105; 
  toco[0] = 0x10;  toco[1] = 0x20;  toco[2] = 0x30;  toco[3] = 0x40;
  qfhr[0] = 0x02;  qfhr[1] = 0x02;  qfhr[2] = 0x01;  qfhr[3] = 0x01; 

  serialStub->sendMeasurement(fhr, mhr, toco, qfhr);

  fhr[0] = 0x0200; fhr[1] = 0x0100; fhr[2] = 0x0080; fhr[3] = 0x0040; 
  mhr[0] = 0x0002; mhr[1] = 0x0004; mhr[2] = 0x0008; mhr[3] = 0x0010; 
  toco[0] = 0x10;  toco[1] = 0x20;  toco[2] = 0x30;  toco[3] = 0x40;
  qfhr[0] = 0x01;  qfhr[1] = 0x02;  qfhr[2] = 0x02;  qfhr[3] = 0x02; 

  serialStub->sendMeasurement(fhr, mhr, toco, qfhr);
  serialStub->sendMarkerEvent();

  ASSERT_DO_ENTER_STATUS_BEFORE_TIMEOUT("buttonPressedEvent") << "Button pressed event was not received";

  monicaStub->sendStop();

  ASSERT_DO_ENTER_STATUS_BEFORE_TIMEOUT("DISCONNECTED")     << "Monica module did not disconnect";

  vector<string> sl = monicaStub->getStatusList();
  shared_ptr<Measurement> measurement = measurementStub->getMeasurement();

  TEST_FOOTER;
    
  // Test list of status events
  ASSERT_EQ(sl.size(), 15)                                  << "Unexpected number of status events received";
  // sl[0]:  "CONNECTING"
  // sl[1]:  "CONNECTED"
  // sl[2]:  "leadcheckStartedEvent"
  // sl[3]:  "leadcheckObservation"
  // sl[4]:  "leadcheckStoppedEvent"
  // sl[5]:  "recordingStartedEvent"
  ASSERT_TRUE(contains(sl[6], "qualityObservation\":\"RED"));
  ASSERT_TRUE(contains(sl[7], "qualityObservation\":\"YELLOW"));
  ASSERT_TRUE(contains(sl[8], "buttonPressedEvent"));
  ASSERT_TRUE(contains(sl[9], "qualityObservation\":\"YELLOW"));
  ASSERT_TRUE(contains(sl[10], "qualityObservation\":\"GREEN"));
  ASSERT_TRUE(contains(sl[11], "buttonPressedEvent"));  
  // sl[12]:  "recordingStoppedEvent"
  // sl[13]:  "DISCONNECTING"
  // sl[14]:  "DISCONNECTED"

  // Test the measurement results
  json dev, obs;
  validateMeasurement(*measurement, dev, obs);


  // Read effectivePeriod (recording start and end time)
  time_t startTime, endTime;
  tm calTime;
  calTime.tm_isdst = -1;

  string startTimeString(obs["effectivePeriod"]["start"].get<string>());
  string endTimeString(obs["effectivePeriod"]["end"].get<string>());
  
  char *test = strptime(startTimeString.c_str(), "%FT%T", &calTime);
  string id = "-00-80-98-FE-FF-01-02-03-8450059-250.0-16-0-" + timeFormat(calTime, "%Y%m%dT%H%M%S.00");
  startTime = mktime(&calTime);
  ASSERT_TRUE(test != 0 && strlen(test) == 6)               << "Parse error reading Start time";

  test = strptime(endTimeString.c_str(), "%FT%T", &calTime);
  endTime = mktime(&calTime);
  ASSERT_TRUE(test != 0 && strlen(test) == 6)               << "Parse error reading End time";

  ASSERT_EQ(difftime(endTime, startTime), 4)                << "Bad recording duration";
 
  ASSERT_TRUE(contains(obs["identifier"][0]["value"], id))  << "Bad FHIR Observation literal id";


  // Check and pull apart the containment structure of the master Observation resource
  
  json fhrObs;
  json mhrObs;
  json tocoObs;
  json qfhrObs;
  json pstObs;
  vector<json> markerObs;
  set<string> anchors;

  try {
    bool fhrFound = false, mhrFound = false, tocoFound = false, qfhrFound = false, pstFound = false;
    for (auto containedObs : obs["contained"]) {
      ASSERT_TRUE(anchors.emplace("#" + containedObs["id"].get<string>()).second) << "Duplicate anchor found";

      string code = containedObs["code"]["coding"][0]["code"].get<string>();

      if (code == "149546" && !mhrFound) {
        mhrFound = true;
        mhrObs = containedObs;        
      } else if (code == "8450048" && !fhrFound) {
        fhrFound = true;
        fhrObs = containedObs;
      } else if (code ==  "8450049" && !tocoFound) {
        tocoFound = true;
        tocoObs = containedObs;
      } else if (code ==  "8450053" && !qfhrFound) {
        qfhrFound = true;
        qfhrObs = containedObs;
      } else if (code ==  "8450054" && !pstFound) {
        pstFound = true;
          pstObs = containedObs;
      } else if (code ==  "8450051") {
        markerObs.push_back(containedObs);
      } else {
        FAIL()      << "Unexpected contained Observation type";
      }
    }
    
    ASSERT_TRUE(fhrFound && mhrFound && tocoFound && qfhrFound && pstFound)  << "Missing contained Observation(s)";


    for (auto member : obs["hasMember"]) {
      ASSERT_EQ(anchors.erase(member["reference"].get<string>()), 1) << "Unlinked contained resource found in master Observation";
    }
    ASSERT_EQ(anchors.size(), 0)                            << "Unresolved master Observation hasMember reference";
    
  } catch (...) {
    FAIL()                                                  << "Error in the FHIR Observation containment structure";
  }

  
  // Test the contents of FHR

  // id, startTime, endTime, datasamples

  ASSERT_EQ(fhrObs["effectivePeriod"]["start"].get<string>(), obs["effectivePeriod"]["start"].get<string>()) <<
    "Fetal Heartrate Observation start time mismatch";
  ASSERT_EQ(fhrObs["effectivePeriod"]["end"].get<string>(), obs["effectivePeriod"]["end"].get<string>()) <<
    "Fetal Heartrate Observation end time mismatch";
  ASSERT_EQ(fhrObs["valueSampledData"]["data"].get<string>(), "0 1 2 3 544 545 546 547 257 257 257 257 512 256 128 64") <<
    "Fetal Heartrate Observation data mismatch";
  
  // Test the contents of MHR

  ASSERT_EQ(mhrObs["effectivePeriod"]["start"].get<string>(), obs["effectivePeriod"]["start"].get<string>()) <<
    "Maternal Heartrate Observation start time mismatch";
  ASSERT_EQ(mhrObs["effectivePeriod"]["end"].get<string>(), obs["effectivePeriod"]["end"].get<string>()) <<
    "Maternal Heartrate Observation end time mismatch";
  ASSERT_EQ(mhrObs["valueSampledData"]["data"].get<string>(), "0 1 2 3 257 257 257 257 258 259 260 261 2 4 8 16") <<
    "Maternal Heartrate Observation data mismatch";
  
  // Test the contents of TOCO

  ASSERT_EQ(tocoObs["effectivePeriod"]["start"].get<string>(), obs["effectivePeriod"]["start"].get<string>()) <<
    "Toco Observation start time mismatch";
  ASSERT_EQ(tocoObs["effectivePeriod"]["end"].get<string>(), obs["effectivePeriod"]["end"].get<string>()) <<
    "Toco Observation end time mismatch";
  ASSERT_EQ(tocoObs["valueSampledData"]["data"].get<string>(), "0 1 2 3 1 1 1 1 16 32 48 64 16 32 48 64") <<
    "Toco Observation data mismatch";
  
  // Test the contents of Q-FHR

  ASSERT_EQ(qfhrObs["effectivePeriod"]["start"].get<string>(), obs["effectivePeriod"]["start"].get<string>()) <<
    "Signal Quality Observation start time mismatch";
  ASSERT_EQ(qfhrObs["effectivePeriod"]["end"].get<string>(), obs["effectivePeriod"]["end"].get<string>()) <<
    "Signal Quality Observation end time mismatch";
  ASSERT_EQ(qfhrObs["valueSampledData"]["data"].get<string>(), "0 1 2 0 1 1 1 1 2 2 1 1 1 2 2 2") <<
    "Signal Quality Observation data mismatch";
  
  // Test the contents of Patient Status

  ASSERT_EQ(pstObs["effectivePeriod"]["start"].get<string>(), obs["effectivePeriod"]["start"].get<string>()) <<
    "Patient Status Observation start time mismatch";
  ASSERT_EQ(pstObs["effectivePeriod"]["end"].get<string>(), obs["effectivePeriod"]["end"].get<string>()) <<
    "Patient Status Observation end time mismatch";
  ASSERT_EQ(pstObs["valueCodeableConcept"]["coding"][0]["code"].get<string>(), "8450058") <<
    "Patient Status Observation code mismatch";
  ASSERT_EQ(pstObs["valueCodeableConcept"]["coding"][0]["display"].get<string>(), "MDC_CTG_PLACEHOLDER_PATIENT_STATUS_UNKNOWN") <<
    "Patient Status Observation display name mismatch";

  // Test the contents of marker events

  ASSERT_EQ(markerObs.size(), 2);

  test = strptime(markerObs[0]["effectiveDateTime"].get<string>().c_str(), "%FT%T", &calTime);
  time_t markerTime = mktime(&calTime);
  ASSERT_TRUE(test != 0 && strlen(test) == 6)               << "Parse error reading marker Observation time";

  ASSERT_EQ(difftime(markerTime, startTime),2)              << "Marker Observation time mismatch";

  test = strptime(markerObs[1]["effectiveDateTime"].get<string>().c_str(), "%FT%T", &calTime);
  markerTime = mktime(&calTime);
  ASSERT_TRUE(test != 0 && strlen(test) == 6)               << "Parse error reading marker Observation time";

  ASSERT_EQ(difftime(markerTime, startTime),4)              << "Marker Observation time mismatch";

}



