#include <iostream>
#include <gtest/gtest.h>
#include "monica-ctg.hpp"

const char *helloWorld = "Hello world!";

TEST(MonicaCtgModule, HelloWorld) {
    EXPECT_STREQ(helloWorld, "Hello world!");
}
