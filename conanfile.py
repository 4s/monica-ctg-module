from conans import ConanFile, CMake, tools


class MonicaCTGmoduleConan(ConanFile):
    name = "monica-ctg-module"
    version = "0.1.1"
    license = "Apache-2.0"
    author = "Jacob Andersen <jacob.andersen@alexandra.dk>"
    url = "https://bitbucket.org/4s/monica-ctg-module"
    description = "Monica AN24 CTG device integration module."
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = {"shared": False}
    generators = "cmake"
    exports_sources = "src/*", "include/*", "CMakeLists.txt", "test/*"
    requires = "phg-messages/0.2.5@_/_", "phg-native-baseplate/0.1.4@_/_"

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()
        if tools.get_env("CONAN_RUN_TESTS", True):
            cmake.test()

    def build_requirements(self):
        if tools.get_env("CONAN_RUN_TESTS", True):
            self.build_requires("gtest/1.10.0")
            self.build_requires("nlohmann_json/3.7.3")

    def package(self):
        self.copy("*", dst="include", src="include")
        self.copy("*messages.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)
