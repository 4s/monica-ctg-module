/*
 *   Copyright 2018 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */


/**
 * @file
 * @brief TEMPORARY interface classes
 *
 * This temporary hand-made implementation declares the interfaces of
 * the Monica CTG module, implementing serializing and de-serializing
 * messages. The intention is to replace all interface boiler plate by
 * auto-generated code in the near future.
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2018 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */


#include "SerialPort_interface.hpp"
#include "CTGControl_interface.hpp"
#include <string>
#include "FHIRMeasurement.pb.h"
#include "DeviceControl.pb.h"
#include "BluetoothDeviceControl.pb.h"
#include "Monica.pb.h"
#include "SerialPort.pb.h"
#include "Error.pb.h"

using std::string;
using std::pair;
using std::vector;
using s4::BasePlate::peer_t;
using s4::BasePlate::objId_t;
using s4::BasePlate::CoreError;
using namespace s4::messages::interfaces;
using namespace s4::messages::classes;

namespace s4 { namespace monica_ctg
{

  static string deviceType = "MDC585728";

  
  CTGControlOutlet::CTGControlOutlet() {
    addFunction<monica::C2P>("Monica.C2P",
               [this](BasePlate::MetaData metadata, monica::C2P* c2p) -> void {
                       if (c2p->has_event()) {
                         auto event = c2p->event();
                         if (event.has_start()) {
                           vector<pair<uint32_t, uint32_t>> properties;
                           for (int i = 0; i < event.start().settings_size(); i++) {
                             auto property = event.start().settings(i);
                             properties.emplace_back(property.attribute(), property.value());
                           }
                           this->startRecording(metadata.sender, properties);
                         } else if (event.has_stop()) {
                           this->stopRecording(metadata.sender);
                         }
                       }
                  });
  }

  void CTGControlOutlet::startRecordingSuccess(peer_t peer) {
    //sendUnicast(peer, "CTGControlPlug", "startRecordingSuccess");
  }
    
  void CTGControlOutlet::startRecordingFailed(peer_t peer, CoreError err) {
    //s4::BasePlate::ErrorMessage error = s4::BasePlate::ErrorMessage();
    //error.set_msg(err);
    //sendUnicast(peer, "CTGControlPlug", "startRecordingFailed", &error);
  }
    
  void CTGControlOutlet::stopRecordingSuccess(peer_t peer, string fhirObservation, string fhirDevice) {
    auto mr = new fhir_measurement::MeasurementReceived();
    mr->set_device_type(deviceType);
    mr->set_fhir_observation(fhirObservation);
    mr->set_fhir_device(fhirDevice);
    auto event = new fhir_measurement::P2C::Event();
    event->set_allocated_measurement_received(mr);
    auto msg = fhir_measurement::P2C();
    msg.set_allocated_event(event);
    sendMulticast("FHIRMeasurement.P2C", "N/A", &msg);
  }
  
  void CTGControlOutlet::stopRecordingFailed(peer_t peer, CoreError err) {
    //s4::BasePlate::ErrorMessage error = s4::BasePlate::ErrorMessage();
    //error.set_msg(err);
    //sendUnicast(peer, "CTGControlPlug", "stopRecordingFailed", &error);
  }
    
  void CTGControlOutlet::statusChanged(peer_t peer, string statusText) {
    auto cs = new monica::CurrentStatus();
    cs->set_current_status(statusText);
    auto event = new monica::P2C::Event();
    event->set_allocated_current_status(cs);
    auto msg = monica::P2C();
    msg.set_allocated_event(event);
    sendMulticast("Monica.P2C", "N/A", &msg);
  }

  SerialPortPlugC::SerialPortPlugC() {
    addFunction<serial_port::P2C>("SerialPort.P2C",
        [this](BasePlate::MetaData metadata, serial_port::P2C* p2c) -> void {
            if (p2c->has_event()) {
              auto event = p2c->event();
              if (event.has_closed()) {
                this->closed(metadata.sender, event.closed().serial_port_handle());
              }
            }
        });
    addFunction<serial_port::OpenSuccess>("#0001_OK.",
        [this](BasePlate::MetaData metadata, serial_port::OpenSuccess* openSuccess) -> void {
            if (openSuccess->message().length() > 0) {
              this->read(openSuccess->message());
            } else {
              this->openSuccess();
            }
        });
    addFunction<error::Error>("#0001_ERR.",
        [this](BasePlate::MetaData metadata, error::Error* error) -> void {
            this->openFailed(error->message());
        });
    addFunction<serial_port::WriteSuccess>("#0002_OK.",
        [this](BasePlate::MetaData metadata, serial_port::WriteSuccess* writeSuccess) -> void {
                                             this->writeSuccess();
        });
    addFunction<error::Error>("#0002_ERR.",
        [this](BasePlate::MetaData metadata, error::Error* error) -> void {
                                this->writeFailed(error->message());
        });
    addFunction<device_control::P2C>("DeviceControl.P2C",
        [this](BasePlate::MetaData metadata, device_control::P2C* p2c) -> void {
            if (p2c->has_event()) {
              auto event = p2c->event();
              if (event.has_search_initiated()) {
                device_control::SearchInitiated si = event.search_initiated();
                int count = si.found_so_far_size();
                for (int i = 0; i < count; i++) {
                  device_control::TransportBundle tb = si.found_so_far(i);
                  if (tb.has_device_transport()) {
                    this->searchResult(si.search_id(), tb.device_transport());
                  }
                }
              } else if (event.has_search_result()) {
                device_control::SearchResult sr = event.search_result();
                if (sr.has_result()) {
                  device_control::TransportBundle tb = sr.result();
                  if (tb.has_device_transport()) {
                    this->searchResult(sr.search_id(), tb.device_transport());
                  }
                }
              }
            }
        });
    addFunction<time::P2C>("Time.P2C",
        [this](BasePlate::MetaData metadata, time::P2C* p2c) -> void {
            if (p2c->has_event()) {
              auto event = p2c->event();
              if (event.has_announce_clock()) {
                this->announceClock(metadata.sender);
              }
            }
        });
    addFunction<time::StartTimerSuccess>("#0003_OK.",
        [this](BasePlate::MetaData metadata, time::StartTimerSuccess* startSuccess) -> void {
            this->startTimerSuccess(startSuccess->timer_id(), startSuccess->fired());
        });
    addFunction<error::Error>("#0003_ERR.",
        [this](BasePlate::MetaData metadata, error::Error* error) -> void {
            this->startTimerFailed(error->message());
        });
    addFunction<time::StopTimerSuccess>("#0004_OK.",
        [this](BasePlate::MetaData metadata, time::StopTimerSuccess* stopSuccess) -> void {
            this->stopTimerSuccess();
        });
    addFunction<error::Error>("#0004_ERR.",
        [this](BasePlate::MetaData metadata, error::Error* error) -> void {
            this->stopTimerFailed(error->message());
        });
    addFunction<time::ReadTimeSuccess>("#0005_OK.",
        [this](BasePlate::MetaData metadata, time::ReadTimeSuccess* readSuccess) -> void {
            if (readSuccess->has_parts()) {
              this->readTimeSuccess(readSuccess->utc(), readSuccess->parts());
            } else {
              this->readTimeSuccess(readSuccess->utc(), readSuccess->iso8601ext());
            }
        });
    addFunction<error::Error>("#0005_ERR.",
        [this](BasePlate::MetaData metadata, error::Error* error) -> void {
            this->readTimeFailed(error->message());
        });
  }

  void SerialPortPlugC::open(peer_t peer, objId_t port) {
    auto open = new serial_port::Open();
    open->set_serial_port_handle(port);
    auto request = new serial_port::C2P::Request();
    request->set_allocated_open(open);
    auto msg = serial_port::C2P();
    msg.set_allocated_request(request);
    sendUnicastWithCallback(peer, "SerialPort.C2P", "N/A", "0001", &msg);
  }
  
  void SerialPortPlugC::close(peer_t peer, objId_t port) {
    auto close = new serial_port::Close();
    close->set_serial_port_handle(port);
    auto event = new serial_port::C2P::Event();
    event->set_allocated_close(close);
    auto msg = serial_port::C2P();
    msg.set_allocated_event(event);
    sendUnicast(peer, "SerialPort.C2P", "N/A", &msg);
  }

  void SerialPortPlugC::write(peer_t peer, objId_t port, string buf, bool flush) {
    auto write = new serial_port::Write();
    write->set_serial_port_handle(port);
    write->set_message(buf);
    write->set_flush(true);
    auto request = new serial_port::C2P::Request();
    request->set_allocated_write(write);
    auto msg = serial_port::C2P();
    msg.set_allocated_request(request);
    sendUnicastWithCallback(peer, "SerialPort.C2P", "N/A", "0002", &msg);
  }

  void SerialPortPlugC::searchStart(objId_t search_id) {
    auto search = new device_control::Search();
    search->set_search_id(search_id);
    auto event = new device_control::C2P::Event();
    event->set_allocated_search(search);
    auto msg = device_control::C2P();
    msg.set_allocated_event(event);
    sendMulticast("DeviceControl.C2P", "N/A", &msg);
  }

  void SerialPortPlugC::searchStop(objId_t search_id) {
    auto stop = new device_control::StopSearch();
    stop->set_search_id(search_id);
    auto event = new device_control::C2P::Event();
    event->set_allocated_stop_search(stop);
    auto msg = device_control::C2P();
    msg.set_allocated_event(event);
    sendMulticast("DeviceControl.C2P", "N/A", &msg);
  }

  void SerialPortPlugC::setStandardPasskey(objId_t handle, string passkey) {
    auto ssp = new bluetooth_device_control::SetStandardPasskey();
    ssp->set_handle(handle);
    ssp->set_passkey(passkey);
    auto event = new bluetooth_device_control::C2P::Event();
    event->set_allocated_set_standard_passkey(ssp);
    auto msg = bluetooth_device_control::C2P();
    msg.set_allocated_event(event);
    sendMulticast("BluetoothDeviceControl.C2P", "N/A", &msg);
  }

  void SerialPortPlugC::solicitClock() {
    auto solicit = new time::SolicitClock();
    auto event = new time::C2P::Event();
    event->set_allocated_solicit_clock(solicit);
    auto msg = time::C2P();
    msg.set_allocated_event(event);
    sendMulticast("Time.C2P", "N/A", &msg);
  }
  
  void SerialPortPlugC::startTimer(peer_t peer, uint32_t delayMillis, bool repeat) {
    auto startTimer = new time::StartTimer();
    startTimer->set_delay_millis(delayMillis);
    startTimer->set_repeat(repeat);
    auto request = new time::C2P::Request();
    request->set_allocated_start_timer(startTimer);
    auto msg = time::C2P();
    msg.set_allocated_request(request);
    sendUnicastWithCallback(peer, "Time.C2P", "N/A", "0003", &msg);
  }
  
  void SerialPortPlugC::stopTimer(peer_t peer, int32_t timerId) {
    auto stopTimer = new time::StopTimer();
    stopTimer->set_timer_id(timerId);
    auto request = new time::C2P::Request();
    request->set_allocated_stop_timer(stopTimer);
    auto msg = time::C2P();
    msg.set_allocated_request(request);
    sendUnicastWithCallback(peer, "Time.C2P", "N/A", "0004", &msg);
  }
  
  void SerialPortPlugC::readTime(peer_t peer, bool utc, time::ReadTime_Format format) {
    auto readTime = new time::ReadTime();
    readTime->set_utc(utc);
    readTime->set_format(format);
    auto request = new time::C2P::Request();
    request->set_allocated_read_time(readTime);
    auto msg = time::C2P();
    msg.set_allocated_request(request);
    sendUnicastWithCallback(peer, "Time.C2P", "N/A", "0005", &msg);
  }

}} // Namespace
