/*
 *   Copyright 2018 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */


/**
 * @file
 * @brief Monica AN24 communication module
 *
 * This module is a proof-of-concept implementation of a communication
 * module capable of performing a CTG recording on a Monica CTG device
 * and outputting a FHIR bundle containing the result.
 *
 * This module should be split into several modules, one translating
 * from the CTG device to the IEEE domain information model and one
 * for translating generic DIM objects to a FHIR bundle and/or FHIR
 * server upload.
 *
 * The overall state is governed by the state machine illustrated by
 * the drawing 
 * 
 * \image html monica_ctg_statesdrawing.pdf
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2018 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */


#include "monica-ctg.hpp"

#include <ctime>
#include <functional>
#include <list>
#include <sstream>
#include <iomanip>
#include <string>
#include <vector>


using s4::BasePlate::peer_t;
using s4::BasePlate::objId_t;
using s4::BasePlate::CoreError;
using s4::BasePlate::ApplicationState;
using s4::BasePlate::Context;
using s4::BasePlate::ModuleBase;
using namespace s4::messages::classes::device;
using namespace s4::messages::interfaces::time;
using std::function;
using std::string;
using std::stringstream;
using std::list;
using std::pair;
using std::vector;



namespace s4 { namespace monica_ctg
{

  
  /**********************************************************************/
  /*                                                                    */
  /*              Declare Monica CTG module private parts               */
  /*                                                                    */
  /**********************************************************************/


  class MonicaCTG::Underpants {
    friend class MonicaCTG;
    
  private:

    Underpants(MonicaCTG &parent);
    
    // CTGControlOutlet implementation
    
    void startRecording(peer_t peer, vector<pair<uint32_t, uint32_t>> properties);
    void stopRecording(peer_t peer);

    // SerialPortPlug implementation
    
    void openSuccess();
    void openFailed(CoreError error);
    void closed(peer_t peer, objId_t port);
    void writeSuccess();
    void writeFailed(CoreError error);
    void read(string input);
    void searchResult(objId_t search_id,
                      DeviceTransport device);
    void announceClock(BasePlate::peer_t peer);
    void readTimeSuccess(bool utc, std::string iso8601ext);
    void readTimeSuccess(bool utc, ReadTimeSuccess_TimeParts parts);
    void readTimeFailed(BasePlate::CoreError error);
    void startTimerSuccess(int32_t timerId, bool fired);
    void startTimerFailed(BasePlate::CoreError error);
    void stopTimerSuccess();
    void stopTimerFailed(BasePlate::CoreError error);

    // Parent object
    MonicaCTG &parent;

    // FHIR Resources generator

    vector<string> generateFHIR();
    
    // Monica communication protocol
    
    void crcCalc(char x, uint16_t &crc);
    // PHG -> Monica
    void sendMsg(vector<char> &msg);
    void sendNote(string payload);
    // Monica -> PHG
    void dispatchInputMsg(vector<char> &msg);
    void handleNoteMsg(vector<char>::const_iterator payload,
                       vector<char>::const_iterator end);
    
    // Helper functions

    bool stateOpenSession();
    void sessionReport();
    void resetFailTimer();
    void searchTimeout();
    
    // Status reporting helper functions
    
    typedef enum {DISCONNECTED, CONNECTING, CONNECTED, DISCONNECTING} connState;
    void reportConnState(connState newState);
    
    typedef enum {
      LEADCHECK_STARTED,
      LEADCHECK_STOPPED,
      RECORDING_STARTED,
      RECORDING_STOPPED,
      BUTTON_PRESSED,
      BATTERY_LOW
    } eventType;
    void reportEvent(eventType event);
    
    typedef enum {
      OK,
      NOT_GOOD,
      BAD
    } leadStatus;

    string leadStatusName(leadStatus s) {
      if (s == OK) return "\"OK\"";
      if (s == NOT_GOOD) return "\"NG\"";
      return "\"BAD\"";
    }

    leadStatus leadStatusValue(uint8_t is1, uint8_t is2) {
      return is1? (is2 ? BAD : NOT_GOOD) : OK;
    }
    
    void reportLeadCheck(leadStatus green, leadStatus white, leadStatus orange,
                         leadStatus yellow, leadStatus black);
    void reportLeadFail(leadStatus cap, leadStatus green, leadStatus white,
                        leadStatus orange, leadStatus yellow);

    typedef enum {RED, YELLOW, GREEN} quality;
    void reportQuality(quality q);

    void reportSNR(uint16_t signal, uint16_t noise);
    
    //
    // Internal data structure to hold the recording
    //

    class CTGDevice {
    public:
      string manufacturer = "Monica Healthcare";
      string continuaRev = "07.00";
      string id, protocolRev, swRev, fwRev, serialNo, btMac;
    } device;



    
    class CTGSample {
    public:
      uint16_t mhr, fhr; 
      uint8_t toco;
    };

    class CTGObservation {
    public:
      time_t startTime;
      unsigned int uaDelay;
      enum {ANTENATAL, INDUCTION, LABOUR, UNKNOWN} patientStatus;
      list<CTGSample> samples;  // Raw data from the Monica C-packets
      list<unsigned int> markers; // Sample index of marker time
      // (marker relates to sample # in the samples list)
    } observation;
    
    
    
    //
    // Overall state of the module. See the state machine graph for
    // details (docs-src/graphics/monica_ctg_statesdrawing.pdf).
    //

    enum {
      NOT_CONNECTED,
      OPENING,
      OPEN,
      GET_VERSION,
      GET_VERSION_WR,
      GET_VERSION_RD,
      IMPEDANCE,
      IMPEDANCE_WR,
      IMPEDANCE_RD,
      MODE,
      READY,
      MONITORING,
      HALT_PENDING,
      HALTING,
      OFF,
      CLOSING,
      END_STOPPED,
      END_PENDING,
      END_RECEIVED,
      END_OFF,
      END_CLOSING,
      STOP_PENDING,
      WRITE_PENDING,
      S_W_PENDING
    } state;

    // Used to communicate internal sub-state between states. 
    int lastResult;

    // Failed leads during recording
    bool capFail, greenFail, whiteFail, orangeFail, yellowFail;

    
    //
    // Timer state and settings
    //

// Wait 5000 ms when searching for bonding Monica devices
#define SEARCH_DELAY 5000
// Wait 2000 ms when communicating with the Monica device
#define RESPONSE_DELAY 2000
// Wait 500 ms when collecting lead fails
#define LEADFAIL_DELAY 500
    
    // The timer module - and whether it is valid or not
    peer_t timer;
    bool timerValid = false;

    enum {
      TIMER_IDLE,
      TIMER_IDLE_STOP_PENDING,
      TIMER_IDLE_START_PENDING,
      TIMER_STARTING,
      TIMER_RESET_PENDING,
      TIMER_RESTART_PENDING,
      TIMER_RUNNING
    } timerState = TIMER_IDLE;
    int32_t currentTimerId = -1;
    uint32_t currentDelay;
    function<void()> currentCallback;
  
    void setTimer(uint32_t delayMillis, function<void()> callback);
    void cancelTimer();


    
    //
    // CTGControl interface state
    //
    
    // In all states except NOT_CONNECTED, the following values are
    // valid
    peer_t master;

    // search_id used for DeviceControl searching for the Monica in
    // the OPENING state.
    objId_t searchId = 0;
    bool searchActive = false;

    //
    // SerialPort interface state, and Philips link layer protocol
    // state
    //
    
    // In all states except NOT_CONNECTED, OPENING, STOP_PENDING,
    // END_CLOSING, and CLOSING the following values are valid
    
    peer_t sessionHandler = "";
    objId_t sessionHandle = 0;
    string btName = "";
    string btMAC = "";

    list<char> inputBuffer;
    uint16_t inputCRC;
    enum {
      DLE,
      STX,
      DATA,
      DATADLE,
      CRC1,
      CRC2
    } inputState;

  };


  
  //
  // Constructor
  //
  
  MonicaCTG::Underpants::Underpants(MonicaCTG &parent) : parent(parent) {
    state = NOT_CONNECTED;
  }
  
  
  /**********************************************************************/
  /*                                                                    */
  /*                 CTGControlOutlet implementation                    */
  /*                                                                    */
  /**********************************************************************/

  void MonicaCTG::Underpants::startRecording(peer_t peer, vector<pair<uint32_t, uint32_t>> properties) {
    if (state == NOT_CONNECTED) {
      // Check if properties contains a setting for the patient status
      // (MDC code 8450054)

      observation.patientStatus = CTGObservation::UNKNOWN;
      for (auto property : properties) {
        if (property.first == 8450054) {
          switch (property.second) {
          case 8450055: // MDC_CTG_PLACEHOLDER_PATIENT_STATUS_ANTENATAL
            observation.patientStatus = CTGObservation::ANTENATAL;
            break;
          case 8450056: // MDC_CTG_PLACEHOLDER_PATIENT_STATUS_INDUCTION
            observation.patientStatus = CTGObservation::INDUCTION;
            break;
          case 8450057: // MDC_CTG_PLACEHOLDER_PATIENT_STATUS_LABOUR
            observation.patientStatus = CTGObservation::LABOUR;
            break;
          case 8450058: // MDC_CTG_PLACEHOLDER_PATIENT_STATUS_UNKNOWN
            break;
          default:
            parent.startRecordingFailed(peer, "Illegal properties argument");
            return;
          }
          break;
        }
      }

      master = peer;
      observation.samples.clear();
      observation.markers.clear();
      state = OPENING;
      searchId++; // Fresh value
      searchActive = true;
      reportConnState(CONNECTING);
      if (timerValid) {
        setTimer(SEARCH_DELAY, [this]()->void {searchTimeout();});
        parent.searchStart(searchId);
      } else {
        // solicit timer
        parent.solicitClock();
      }
    } else {
      parent.startRecordingFailed(peer, "Already running, please come back later");
    }
  }

  void MonicaCTG::Underpants::stopRecording(peer_t peer) {
    if (peer != master) {
      parent.stopRecordingFailed(peer, "You are not in an active session with me!");
      return;
    }
    switch (state) {
    case OPENING:
      if (!timerValid) {
        // stopRecording received between SolicitClock->AnnounceClock
      } else if (searchActive) {
        // stopRecording received between searchStart->result
        cancelTimer();
        parent.searchStop(searchId);
        searchActive = false;
      } else {
        parent.close(sessionHandler,sessionHandle);
        break;
      }
      state = NOT_CONNECTED;
      reportConnState(DISCONNECTED);
      parent.startRecordingFailed(master, CoreError("Recording stopped"));
      break;
    case STOP_PENDING:
      sessionReport();
      break;
    case S_W_PENDING:
      state = WRITE_PENDING;
      break;
    case END_CLOSING:
      state = CLOSING;
      break;
    case END_OFF:
      state = OFF;
      break;
    case END_RECEIVED:
      state = HALTING;
      break;
    case END_PENDING:
      state = END_STOPPED;
      break;
    case READY:
      reportConnState(DISCONNECTING);
      state = HALT_PENDING;
      break;
    case MONITORING:
      cancelTimer();
      reportEvent(RECORDING_STOPPED);
      reportConnState(DISCONNECTING);
      state = HALTING;
      // Send "Halt" message
      {
        vector<char> msg({'H'});
        sendMsg(msg);
      }
      break;
    case IMPEDANCE_WR:
      reportEvent(LEADCHECK_STOPPED);
      reportConnState(DISCONNECTING);
      // No break here!
    case GET_VERSION_WR:
      state = OFF;
      cancelTimer();
      sendNote("OFD");
      break;
    case IMPEDANCE:
    case IMPEDANCE_RD:
      reportEvent(LEADCHECK_STOPPED);
      // No break here!
    case MODE:
      reportConnState(DISCONNECTING);
      // No break here!
    case OPEN:
    case GET_VERSION:
    case GET_VERSION_RD:
      state = HALTING;
      break;
    default:
      parent.stopRecordingFailed(peer, "Not running");
      return;
    }
    parent.dbg("Stopping recording...");
  }
  
  
  
  
  /**********************************************************************/
  /*                                                                    */
  /*                   SerialPortPlug implementation                     */
  /*                                                                    */
  /**********************************************************************/
  
  void MonicaCTG::Underpants::openSuccess() {
    if (state == OPENING) {
      state = OPEN;
      inputState = DLE;
      parent.dbg("Monica connection open");
      
      lastResult = 1; // 1: Halt message, 0: DEL message

      // Send "Halt" message
      vector<char> msg({'H'});
      sendMsg(msg);
    
      parent.startRecordingSuccess(master);
    }
  }

  void MonicaCTG::Underpants::openFailed(CoreError error) {
    if (state == OPENING) {
      state = NOT_CONNECTED;
      reportConnState(DISCONNECTED);
      parent.startRecordingFailed(master,error);
    }
  }

  void MonicaCTG::Underpants::closed(peer_t peer, objId_t port) {
    // Unrelated closed?
    if (peer != sessionHandler || port != sessionHandle) return;

    parent.dbg("Monica connection closed");
    switch (state) {
    case CLOSING:
      sessionReport();
      return;
    case OFF:
    case HALTING:
    case HALT_PENDING:
    case END_STOPPED:
      state = WRITE_PENDING;
      return;
    case NOT_CONNECTED:
    case OPENING:
    case STOP_PENDING:
    case WRITE_PENDING:
    case S_W_PENDING:
      // Should never happen; ignore
      return;
    case MONITORING:
      cancelTimer();
      reportEvent(RECORDING_STOPPED);
      reportConnState(DISCONNECTING);
      reportConnState(DISCONNECTED);
      state = STOP_PENDING;
      break;    
    case IMPEDANCE_WR:
      reportEvent(LEADCHECK_STOPPED);
      reportConnState(DISCONNECTING);
      // No break here!
    case END_CLOSING:
    case GET_VERSION_WR:
      cancelTimer();
      reportConnState(DISCONNECTED);
      state = STOP_PENDING;
      break;
    case IMPEDANCE:
    case IMPEDANCE_RD:
      reportEvent(LEADCHECK_STOPPED);
    case MODE:
    case READY:
      reportConnState(DISCONNECTING);
      // No break here!
    default: // 6 remaining states
      state = S_W_PENDING;
    }
  }

  void MonicaCTG::Underpants::writeSuccess() {
    vector<char> msg;
    switch (state) {
    case OPEN:
      // Don't send DEL when the Monica is not sending "GOT"s.
      /*    if (lastResult) {
            lastResult = 0;
            sendNote("DEL");
            return;
            }*/ lastResult = 0;
      // lastResult == 0 --  0: version, 1: Patient status, 2: UA delay
      state = GET_VERSION;
      msg = {'?','I'};
      break;
    case GET_VERSION:
      setTimer(RESPONSE_DELAY, [this]() {
          if (state == GET_VERSION_WR) {
            state = END_CLOSING;
            parent.close(sessionHandler,sessionHandle);
          }
        });
      state = GET_VERSION_WR;
      return;
    case GET_VERSION_RD:
      switch (lastResult) {
      case 1:
        state = GET_VERSION;
        sendNote("PST\001\0");
        return;
      case 2:
        state = GET_VERSION;
        sendNote("UAD");
        return;
      case 3:
        state = IMPEDANCE;
        reportConnState(CONNECTED);
        reportEvent(LEADCHECK_STARTED);
        parent.dbg("Testing leads...");
        sendNote("IM3");
        return;
      }
      break;
    case IMPEDANCE:
      setTimer(RESPONSE_DELAY, [this]() {
          if (state == IMPEDANCE_WR) {
            state = END_CLOSING;
            parent.close(sessionHandler,sessionHandle);
          }
        });
      state = IMPEDANCE_WR;
      return;
    case IMPEDANCE_RD:
      if (lastResult) {
        state = IMPEDANCE;
        sendNote("IM3");
      } else {
        reportEvent(LEADCHECK_STOPPED);
        state = MODE;
        sendNote("CONT");
      }
      return;
    case MODE:
      state = READY;
      observation.startTime = time(nullptr);
      msg = {'G'};
      parent.dbg("Starting recording...");
      break;      
    case READY:
      reportEvent(RECORDING_STARTED);
      state = MONITORING;
      return;
    case HALT_PENDING:
      msg = {'H'};
      break;
    case HALTING:
      state = OFF;
      sendNote("OFD");
      return;
    case END_STOPPED:
      state = HALTING;
      sendNote("END");
      return;
    case OFF:
      state = CLOSING;
      parent.close(sessionHandler, sessionHandle);
      return;
    case END_PENDING:
      state = END_RECEIVED;
      sendNote("END");
      return;
    case END_RECEIVED:
      state = END_OFF;
      sendNote("OFD");
      return;
    case END_OFF:
      state = END_CLOSING;
      parent.close(sessionHandler, sessionHandle);
      return;
    case S_W_PENDING:
      reportConnState(DISCONNECTED);
      state = STOP_PENDING;
      return;
    case WRITE_PENDING:
      sessionReport();
      return;
    default:
      printf("FIXME\n");
      return;
    }
    sendMsg(msg);  
  }

  void MonicaCTG::Underpants::writeFailed(CoreError error) {
    parent.err(error);
    switch (state) {
    case IMPEDANCE:
    case IMPEDANCE_RD:
      reportEvent(LEADCHECK_STOPPED);
      // No break here!
    case MODE:
    case READY:
      reportConnState(DISCONNECTING);
      // No break here!
    case OPEN:
    case GET_VERSION:
    case GET_VERSION_RD:
    case END_PENDING:
    case END_RECEIVED:
    case END_OFF:
      state = END_CLOSING;
      parent.close(sessionHandler,sessionHandle);
      break;
    case HALT_PENDING:
    case HALTING:
    case END_STOPPED:
    case OFF:
      state = CLOSING;
      parent.close(sessionHandler,sessionHandle);
      break;
    case S_W_PENDING:
      reportConnState(DISCONNECTED);
      state = STOP_PENDING;
      break;
    case WRITE_PENDING:
      sessionReport();
      break;
    default:
      printf("FIXME\n");
      break;
    }
  }

  void MonicaCTG::Underpants::read(string input) {
    if (!stateOpenSession()) {
      return;
    }
    for (char c : input) {
      crcCalc(c,inputCRC);
      switch (inputState) {
      case DLE:
        if (c == 0x10) {
          inputState = STX;
          inputCRC = 0;
          crcCalc(c,inputCRC);
        }
        break;
      case STX:
        if (c == 0x02) {
          inputState = DATA;
          inputBuffer.clear();
        } else {
          inputState = DLE;
        }
        break;
      case DATA:
        if (c == 0x10) {
          inputState = DATADLE;
        } else {
          inputBuffer.push_back(c);
        }
        break;
      case DATADLE:
        switch (c) {
        case 0x10:
          inputState = DATA;
          inputBuffer.push_back(c);
          break;
        case 0x02:
          inputState = DATA;
          inputBuffer.clear();
          inputCRC = 0;
          crcCalc(0x10,inputCRC);
          crcCalc(0x02,inputCRC);
          break;
        case 0x03:
          inputState = CRC1;
          break;
        default: // Bad format - discard
          inputState = DLE;
        }
        break;
      case CRC1:
        inputState = CRC2;
        break;
      case CRC2:
        inputState = DLE;
        if (inputCRC) {
          //TODO: wrn("BAD CRC in input data");
        } else {
          vector<char> msg(inputBuffer.begin(),inputBuffer.end());
          dispatchInputMsg(msg);
        }
        break;
      default:
        inputState = DLE;
        break;
      }
    }
  }

  void MonicaCTG::Underpants::searchResult(objId_t search_id, DeviceTransport dt) {
    // Search not active? Bail out
    if (!searchActive || state != OPENING) return;

    // Must be a Bluetooth device
    if (!dt.has_bluetooth_device()) return;

    BluetoothDevice bd = dt.bluetooth_device();

    // Must have a serial port profile
    bool hasSerial = false;
    int count = bd.bluetooth_profiles_size();
    int i;
    for (i = 0; i < count; i++) {
      if (bd.bluetooth_profiles(i).profile() == BluetoothDevice_BluetoothProfile::SERIAL_PORT_PROFILE) {
        hasSerial = true;
        break;
      }
    }
    if (!hasSerial) return;

    // If this is not a Monica device, bail out
    if (bd.name().substr(0,5) != "AN24 " ||
        bd.bd_addr().substr(0,8) != "00:80:98") {
          return;
    }

    // Save device properties
    sessionHandle = bd.bluetooth_profiles(i).handle();
    btName = bd.name();
    btMAC = bd.bd_addr();
    std::stringstream s;
    s << std::setfill('0') << std::setw(8) << std::hex << bd.bluetooth_profiles(i).handler();
    sessionHandler = s.str();

    // Found Monica! Stop searching
    parent.searchStop(searchId);
    cancelTimer();
    searchActive = false;

    // We set the passkey just in case it is needed (although we are
    // already paired)
    parent.setStandardPasskey(dt.handle(), "22222222");

    // And open port
    parent.open(sessionHandler, sessionHandle);

  }

  void MonicaCTG::Underpants::announceClock(BasePlate::peer_t peer) {
    if (state == OPENING && !timerValid) {
      timerValid = true;
      timer = peer;
      setTimer(SEARCH_DELAY, [this]()-> void {searchTimeout();});
      parent.searchStart(searchId);
    }
  }
  
  void MonicaCTG::Underpants::startTimerSuccess(int32_t timerId, bool fired) {
    if (timerId != currentTimerId) {
      switch (timerState) {
      case TIMER_RESET_PENDING:
        timerState = TIMER_RESTART_PENDING;
        currentTimerId = timerId;
        parent.stopTimer(timer, timerId);
        break;
      case TIMER_STARTING:
        timerState = TIMER_RUNNING;
        currentTimerId = timerId;
        break;
      case TIMER_IDLE_START_PENDING:
        timerState = TIMER_IDLE_STOP_PENDING;
        currentTimerId = timerId;
        parent.stopTimer(timer, timerId);
        break;
      default:
        break;
      }
    }
    if (timerState == TIMER_RUNNING && fired && timerId == currentTimerId) {
      timerState = TIMER_IDLE;
      currentCallback();
    }
  }
  
  void MonicaCTG::Underpants::startTimerFailed(BasePlate::CoreError error) {
    parent.err(error);
    switch (timerState) {
    case TIMER_STARTING:
    case TIMER_RESET_PENDING:
    case TIMER_IDLE_START_PENDING:
      timerState = TIMER_IDLE;
      break;
    default:
      break;
    }
  }

  void MonicaCTG::Underpants::stopTimerSuccess() {
    switch (timerState) {
    case TIMER_RESTART_PENDING:
      timerState = TIMER_STARTING;
      parent.startTimer(timer, currentDelay, false);
      break;
    case TIMER_IDLE_STOP_PENDING:
      timerState = TIMER_IDLE;
      break;
    default:
      break;
    }
  }
  
  void MonicaCTG::Underpants::stopTimerFailed(BasePlate::CoreError error) {
    parent.err(error);
    // ignore
  }

  void MonicaCTG::Underpants::readTimeSuccess(bool utc, std::string iso8601ext) {
    // Currently not used
  }
  void MonicaCTG::Underpants::readTimeSuccess(bool utc, ReadTimeSuccess_TimeParts parts) {
    // Currently not used
  }
  void MonicaCTG::Underpants::readTimeFailed(BasePlate::CoreError error) {
    // Currently not used
  }


  
  /**********************************************************************/
  /*                                                                    */
  /*                       FHIR Bundle generator                        */
  /*                                                                    */
  /**********************************************************************/

  string timeFormat(time_t &time, const char *format) {
    char timestampBuffer[50];
    size_t len = strftime(timestampBuffer,50,format,localtime(&time));
    return string(timestampBuffer, len);
  }

  string timeAsISO(time_t &time) {
    return timeFormat(time,"%FT%T") + ((localtime(&time)->tm_isdst)?"+02:00":"+01:00");
  }

  string timeAsHL7DTM(time_t &time) {
    return timeFormat(time,"%Y%m%dT%H%M%S.00");
  }

  string generateUUID() {
    char buffer[10];
    string uuid = string(36,' ');
  
    for(int i=0;i<12;i++) {
      sprintf(buffer,"%8x",rand());
      uuid[i*3+0] = buffer[5];
      uuid[i*3+1] = buffer[6];
      uuid[i*3+2] = buffer[7];
    }

    uuid[8] = '-';
    uuid[13] = '-';
    uuid[18] = '-';
    uuid[23] = '-';
    uuid[14] = '4';
    uuid[19] = ((uuid[19] & 0x03) > 1) ? (uuid[19] & 0x03) | 'a' : (uuid[19] & 0x03) | '8';

    return uuid;
  }

  vector<string> MonicaCTG::Underpants::generateFHIR() {
    string startTime = timeAsISO(observation.startTime);
    string startTimeCompact = timeAsHL7DTM(observation.startTime);
    time_t et = observation.startTime + observation.samples.size()/4;
    string endTime = timeAsISO(et);

    string patientIdentifier = ""; // faked - we know nothing about the patient.
    string patientIdSystem = "";

    string phdBtMac = device.btMac;
    string phdBtMacDashed = phdBtMac.substr(0,2) + "-" + phdBtMac.substr(2,2) + "-" +
      phdBtMac.substr(4,2) + "-" + phdBtMac.substr(6,2) + "-" +
      phdBtMac.substr(8,2) + "-" + phdBtMac.substr(10,2);
    string phdEUI64 = phdBtMac.substr(0,6) + "FEFF" + phdBtMac.substr(6);
    string phdEUI64dashed = phdEUI64.substr(0,2) + "-" + phdEUI64.substr(2,2) + "-" +
      phdEUI64.substr(4,2) + "-" + phdEUI64.substr(6,2) + "-" +
      phdEUI64.substr(8,2) + "-" + phdEUI64.substr(10,2) + "-" +
      phdEUI64.substr(12,2) + "-" + phdEUI64.substr(14,2);
    string phdManufacturer = device.manufacturer;
    string phdModel = device.id;
    string phdSerial = device.serialNo;
    string phdSwRev = device.swRev;
    string phdFwRev = device.fwRev;
    string phdProtoRev = device.protocolRev;
    string phdContinuaRev = device.continuaRev;

    stringstream fhirobs, fhirdev, containedobs;
    list<CTGSample>::iterator it;
    int obsId = 0;

    // PHD Device
    fhirdev << "{\"resourceType\":\"Device\",\"id\": \"" << phdEUI64
            << "." << phdBtMac << "\",\"meta\":{\"profile\":"
            << "[\"http://hl7.org/fhir/uv/phd/StructureDefinition/PhdDevice\"]},"
            << "\"identifier\":[{\"type\":{\"coding\":[{\"system\":"
            << "\"http://hl7.org/fhir/uv/phd/CodeSystem/ContinuaDeviceIdentifiers"
            << "\",\"code\":\"SYSID\"}]},\"system\":"
            << "\"urn:oid:1.2.840.10004.1.1.1.0.0.1.0.0.1.2680\","
            << "\"value\":\"" << phdEUI64dashed << "\"},{\"type\":{"
            << "\"coding\":[{\"system\":"
            << "\"http://hl7.org/fhir/uv/phd/CodeSystem/ContinuaDeviceIdentifiers\","
            << "\"code\":\"BTMAC\"}]},\"system\":"
            << "\"http://hl7.org/fhir/sid/eui-48\",\"value\": \""
            << phdBtMacDashed << "\"}],\"type\":{\"coding\":"
            << "[{\"system\":\"urn:iso:std:iso:11073:10101\",\"code\":"
            << "\"65573\",\"display\":\"MDC_MOC_VMS_MDS_SIMP\"}],\"text\":"
            << "\"MDC_MOC_VMS_MDS_SIMP: Continua Personal Health Device\"},"
            << "\"specialization\":[{\"systemType\":{\"coding\":[{\"system\":"
            << "\"urn:iso:std:iso:11073:10101\",\"code\":\"585728\",\"display\":"
            << "\"MDC_DEV_PLACEHOLDER_SPEC_PROFILE_CTG\"}],\"text\":"
            << "\"MDC_DEV_PLACEHOLDER_SPEC_PROFILE_CTG: CTG recorder\"},"
            << "\"version\":\"0\"}],\"manufacturer\":\""
            << phdManufacturer << "\",\"modelNumber\":\"" << phdModel
            << "\",\"serialNumber\":\"" << phdSerial << "\",\"version\":"
            << "[{\"type\":{\"coding\":[{\"system\":"
            << "\"urn:iso:std:iso:11073:10101\",\"code\":\"531975\",\"display\":"
            << "\"MDC_ID_PROD_SPEC_SW\"}],\"text\":"
            << "\"MDC_ID_PROD_SPEC_SW: Software revision\"},"
            << "\"value\":\"" << phdSwRev << "\"},{\"type\":{\"coding\":"
            << "[{\"system\":\"urn:iso:std:iso:11073:10101\",\"code\":\"531976\","
            << "\"display\":\"MDC_ID_PROD_SPEC_FW\"}],\"text\":"
            << "\"MDC_ID_PROD_SPEC_FW: Firmware revision\"},\"value\":\""
            << phdFwRev << "\"},{\"type\":{\"coding\":[{\"system\":"
            << "\"urn:iso:std:iso:11073:10101\",\"code\":\"531977\","
            << "\"display\":\"MDC_ID_PROD_SPEC_PROTOCOL\"}],\"text\":"
            << "\"MDC_ID_PROD_SPEC_PROTOCOL: Protocol revision\"},\"value\":\""
            << phdProtoRev << "\"},{\"type\":{\"coding\":[{\"system\":"
            << "\"urn:iso:std:iso:11073:10101\",\"code\":\"532352\","
            << "\"display\":\"MDC_REG_CERT_DATA_CONTINUA_VERSION\"}],"
            << "\"text\":\"MDC_REG_CERT_DATA_CONTINUA_VERSION: "
            << "Continua Design Guidelines version\"},\"value\":\""
            << phdContinuaRev << "\"}],\"property\":[{\"type\":"
            << "{\"coding\":[{\"system\":"
            << "\"http://hl7.org/fhir/uv/phd/CodeSystem/ASN1ToHL7\","
            << "\"code\":\"532354.0\",\"display\":\"regulation-status\"}],"
            << "\"text\":\"Regulation status\"},\"valueCode\":[{\"coding\":"
            << "[{\"system\":\"http://terminology.hl7.org/CodeSystem/v2-0136\","
            << "\"code\":\"N\",\"display\":\"regulated\"}],\"text\":"
            << "\"Regulated medical device\"}]},{\"type\":{\"coding\":["
            << "{\"system\":\"urn:iso:std:iso:11073:10101\",\"code\":\"68220\","
            << "\"display\":\"MDC_TIME_SYNC_PROTOCOL\"}],\"text\":"
            << "\"MDC_TIME_SYNC_PROTOCOL\"},\"valueCode\":[{\"coding\":["
            << "{\"system\":\"urn:iso:std:iso:11073:10101\",\"code\":"
            << "\"532224\",\"display\":\"MDC_TIME_SYNC_NONE\"}],"
            << "\"text\":\"MDC_TIME_SYNC_NONE\"}]}]}\n";



    // Prepare the master observation
    fhirobs << "{\"resourceType\":\"Observation\",\"meta\":{\"profile\":"
            << "[\"https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg"
            << "/pages/115146755/PhdScannerPanelObservation\"]},\"identifier\":"
            << "[{\"value\":\"" << patientIdentifier << "-" << patientIdSystem << "-"
            << phdEUI64dashed << "-8450059-250.0-" << observation.samples.size()
            << "-" << (observation.samples.front().mhr & 0x7FF) << "-"
            << startTimeCompact << "\"}],\"status\":\"final\",\"code\":"
            << "{\"coding\":[{\"system\":\"urn:iso:std:iso:11073:10101\","
            << "\"code\":\"8450059\",\"display\":"
            << "\"MDC_PLACEHOLDER_RECORDING_PANEL_SELF_PERFORMED_CTG\"}],"
            << "\"text\":\"MDC_PLACEHOLDER_RECORDING_PANEL_SELF_PERFORMED_CTG: "
            << "CTG recording\"},\"effectivePeriod\":{\"start\":\"" << startTime
            << "\",\"end\":\"" << endTime << "\"},\"extension\":[{\"url\":"
            << "\"https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg"
            << "/pages/114262030/Observation+dataEntryMethod\",\"valueCoding\":"
            << "{\"code\":\"automatic\","
            << "\"system\":\"https://issuetracker4s.atlassian.net/wiki/spaces/"
            << "FHIRReg/pages/100728837/ObservationDefinition+dataEntryMethod+"
            << "coding+system\",\"display\":\"Automatisk måling\"}}],"
            << "\"hasMember\":[";


    // MHR Observation
    fhirobs << "{\"reference\":\"#childObs" << ++obsId << "\"}";
    containedobs << "{\"resourceType\":\"Observation\",\"id\":\"childObs"
                 << obsId << "\",\"meta\":{\"profile\":["
                 << "\"http://hl7.org/fhir/uv/phd/StructureDefinition/"
                 << "PhdRtsaObservation\"]},\"status\":\"final\",\"code\":"
                 << "{\"coding\":[{\"system\":\"urn:iso:std:iso:11073:10101\","
                 << "\"code\":\"149546\",\"display\":\"MDC_PULS_RATE_NON_INV\"},"
                 << "{\"system\":\"http://loinc.org\",\"code\":\"8867-4\","
                 << "\"display\":\"Heart Rate\"}]},\"effectivePeriod\":"
                 << "{\"start\":\"" << startTime << "\",\"end\":\"" << endTime
                 << "\"},\"valueSampledData\":{\"origin\":{\"value\":0.0,"
                 << "\"unit\":\"bpm\",\"system\":\"http://unitsofmeasure.org\","
                 << "\"code\":\"/min\"},\"period\":250.0,\"factor\":0.25,"
                 << "\"dimensions\":1,\"data\":\"";

    it = observation.samples.begin();
    containedobs << ((it++)->mhr & 0x07FF);
    for (; it != observation.samples.end(); it++) containedobs << " " << (it->mhr & 0x7FF);
    containedobs << "\"},\"referenceRange\":[{\"low\": {\"value\": 0.25,"
                 << "\"system\":\"http://unitsofmeasure.org\",\"code\":\"/min\""
                 << "},\"high\":{\"value\":300.0,\"system\":"
                 << "\"http://unitsofmeasure.org\",\"code\":\"/min\"}}],"
                 << "\"component\":[{\"code\":{\"coding\":[{\"system\":"
                 << "\"urn:iso:std:iso:11073:10101\",\"code\":\"68193\","
                 << "\"display\":\"MDC_ATTR_SUPPLEMENTAL_TYPES\"}]},"
                 << "\"valueCodeableConcept\":{\"coding\":[{\"system\":"
                 << "\"urn:iso:std:iso:11073:10101\",\"code\":\"8450060\","
                 << "\"display\":\"MDC_CTG_PLACEHOLDER_SOURCE_ECG\"}]}}]},";


    // FHR Observation
    fhirobs << ",{\"reference\":\"#childObs" << ++obsId << "\"}";
    containedobs << "{\"resourceType\":\"Observation\",\"id\":\"childObs"
                 << obsId << "\",\"meta\":{\"profile\":"
                 << "[\"http://hl7.org/fhir/uv/phd/StructureDefinition/"
                 << "PhdRtsaObservation\"]},\"status\":\"final\",\"code\":"
                 << "{\"coding\":[{\"system\":\"urn:iso:std:iso:11073:10101\","
                 << "\"code\":\"8450048\",\"display\":"
                 << "\"MDC_CTG_PLACEHOLDER_FETAL_HEART_RATE_INSTANT\"}],"
                 << "\"text\":\"MDC_CTG_PLACEHOLDER_FETAL_HEART_RATE_INSTANT: "
                 << "Fetal heart rate\"},"
                 << "\"effectivePeriod\":{\"start\":\"" << startTime
                 << "\",\"end\":\"" << endTime << "\"},\"valueSampledData\":"
                 << "{\"origin\":{\"value\":0.0,\"unit\":\"bpm\",\"system\":"
                 << "\"http://unitsofmeasure.org\",\"code\":\"/min\"},"
                 << "\"period\":250.0,\"factor\":0.25,\"dimensions\":1,"
                 << "\"data\":\"";

    it = observation.samples.begin();
    containedobs << ((it++)->fhr & 0x07FF);
    for (; it != observation.samples.end(); it++) containedobs << " " << (it->fhr & 0x7FF);

    containedobs << "\"},\"referenceRange\":[{\"low\":{\"value\":0.25,"
                 << "\"system\":\"http://unitsofmeasure.org\",\"code\":\"/min\""
                 << "},\"high\":{\"value\":300.0,\"system\":"
                 << "\"http://unitsofmeasure.org\",\"code\":\"/min\"}}],"
                 << "\"component\":[{\"code\":{\"coding\":[{\"system\":"
                 << "\"urn:iso:std:iso:11073:10101\",\"code\":\"68193\","
                 << "\"display\":\"MDC_ATTR_SUPPLEMENTAL_TYPES\"}]},"
                 << "\"valueCodeableConcept\":{\"coding\":[{\"system\":"
                 << "\"urn:iso:std:iso:11073:10101\",\"code\":\"8450060\","
                 << "\"display\": \"MDC_CTG_PLACEHOLDER_SOURCE_ECG\"}]}}]},";


            
    // Toco Observation
    fhirobs << ",{\"reference\":\"#childObs" << ++obsId << "\"}";
    containedobs << "{\"resourceType\":\"Observation\",\"id\":\"childObs"
                 << obsId << "\",\"meta\":{\"profile\":"
                 << "[\"http://hl7.org/fhir/uv/phd/StructureDefinition/"
                 << "PhdRtsaObservation\"]},\"status\":\"final\",\"code\":"
                 << "{\"coding\":[{\"system\":\"urn:iso:std:iso:11073:10101\","
                 << "\"code\":\"8450049\",\"display\":"
                 << "\"MDC_CTG_PLACEHOLDER_UTERINE_ACTIVITY\"}],\"text\":"
                 << "\"MDC_CTG_PLACEHOLDER_UTERINE_ACTIVITY: Uterine activity\"},"
                 << "\"effectivePeriod\":{\"start\":\"" << startTime
                 << "\",\"end\":\"" << endTime << "\"},\"valueSampledData\":"
                 << "{\"origin\":{\"value\":0.0,\"unit\":\"µV\",\"system\":"
                 << "\"http://unitsofmeasure.org\",\"code\":\"uV\"},"
                 << "\"period\":250.0,\"factor\":1.96,\"dimensions\":1,"
                 << "\"data\":\"";

    if (observation.samples.size() > observation.uaDelay) { 
      it = observation.samples.begin();
      for (int i = 0; i < observation.uaDelay; i++) it++;
      containedobs << (uint16_t)((it++)->toco);
      for (; it != observation.samples.end(); it++) containedobs << " " << (uint16_t)(it->toco);
      for (int i = 0; i < observation.uaDelay; i++) containedobs << " 0";
    } else {
      containedobs << "0";
      for (int i = 1; i < observation.samples.size(); i++) containedobs << " 0";
    }

    containedobs << "\"},\"referenceRange\":[{\"low\":{\"value\":0,"
                 << "\"system\":\"http://unitsofmeasure.org\",\"code\":\"uV\""
                 << "},\"high\":{\"value\":500,\"system\":"
                 << "\"http://unitsofmeasure.org\",\"code\":\"uV\"}}],"
                 << "\"component\":[{\"code\":{\"coding\":[{\"system\":"
                 << "\"urn:iso:std:iso:11073:10101\",\"code\":\"68193\","
                 << "\"display\":\"MDC_ATTR_SUPPLEMENTAL_TYPES\"}]},"
                 << "\"valueCodeableConcept\":{\"coding\":[{\"system\":"
                 << "\"urn:iso:std:iso:11073:10101\",\"code\":\"8450062\","
                 << "\"display\":\"MDC_CTG_PLACEHOLDER_SOURCE_EHG\"}]}}]},";

         

    // FHR Quality Observation
    fhirobs << ",{\"reference\":\"#childObs" << ++obsId << "\"}";
    containedobs << "{\"resourceType\":\"Observation\",\"id\":\"childObs"
                 << obsId << "\",\"meta\":{\"profile\":"
                 << "[\"http://hl7.org/fhir/uv/phd/StructureDefinition/"
                 << "PhdRtsaObservation\"]},\"status\":\"final\",\"code\":"
                 << "{\"coding\":[{\"system\":\"urn:iso:std:iso:11073:10101\","
                 << "\"code\":\"8450053\",\"display\":"
                 << "\"MDC_CTG_PLACEHOLDER_SIGNAL_QUALITY\"}],\"text\":"
                 << "\"MDC_CTG_PLACEHOLDER_SIGNAL_QUALITY: Signal quality\"},"
                 << "\"effectivePeriod\":{\"start\":\"" << startTime
                 << "\",\"end\":\"" << endTime << "\"},\"valueSampledData\":"
                 << "{\"origin\":{\"value\":0.0,\"unit\":\"1\",\"system\":"
                 << "\"http://unitsofmeasure.org\",\"code\":\"1\"},"
                 << "\"period\":250.0,\"factor\":1.00,\"dimensions\":1,"
                 << "\"data\":\"";

    it = observation.samples.begin();
    containedobs << ((it++)->mhr >> 13);
    for (; it != observation.samples.end(); it++) containedobs << " " << (it->mhr >> 13);
    
    containedobs << "\"},\"referenceRange\":[{\"low\":{\"value\":0,\"system\":"
                 << "\"http://unitsofmeasure.org\",\"code\":\"1\"},\"high\":{"
                 << "\"value\":2,\"system\":\"http://unitsofmeasure.org\","
                 << "\"code\":\"1\"}}]},";

         

    // Patient status observation
    string pstTypeCode = (observation.patientStatus == CTGObservation::ANTENATAL) ?
      "8450055" : ((observation.patientStatus == CTGObservation::INDUCTION) ?
                   "8450056" :
                   ((observation.patientStatus == CTGObservation::LABOUR) ?
                    "8450057" : "8450058"));
    string pstTypeName = (observation.patientStatus == CTGObservation::ANTENATAL) ?
      "ANTENATAL" : ((observation.patientStatus == CTGObservation::INDUCTION) ?
                     "INDUCTION" :
                     ((observation.patientStatus == CTGObservation::LABOUR) ?
                      "LABOUR" : "UNKNOWN"));
    fhirobs << ",{\"reference\":\"#childObs" << ++obsId << "\"}";
    containedobs << "{\"resourceType\":\"Observation\",\"id\":\"childObs"
                 << obsId << "\",\"meta\":{\"profile\":"
                 << "[\"http://hl7.org/fhir/uv/phd/StructureDefinition/"
                 << "PhdCodedEnumerationObservation\"]},\"status\":\"final\","
                 << "\"code\":{\"coding\":[{\"system\":"
                 << "\"urn:iso:std:iso:11073:10101\",\"code\":\"8450054\","
                 << "\"display\":\"MDC_CTG_PLACEHOLDER_PATIENT_STATUS\"}],"
                 << "\"text\":\"MDC_CTG_PLACEHOLDER_PATIENT_STATUS: Patient "
                 << "status\"},"
                 << "\"effectivePeriod\":{\"start\":\"" << startTime
                 << "\",\"end\":\"" << endTime << "\"},\"valueCodeableConcept\":"
                 << "{\"coding\":[{\"system\":\"urn:iso:std:iso:11073:10101\","
                 << "\"code\":\"" << pstTypeCode << "\",\"display\":"
                 << "\"MDC_CTG_PLACEHOLDER_PATIENT_STATUS_" << pstTypeName
                 << "\"}]}}";

    // Marker event observations
    for (unsigned int samplePos : observation.markers) {
      // Time calc magic
      tm *marker_tm;
      marker_tm = localtime(&observation.startTime);
      marker_tm->tm_sec += samplePos/4;
      time_t markerTime = mktime(marker_tm);
      startTime = timeAsISO(markerTime);//"2017-06-12T10:48:05+02:00";
      startTimeCompact = timeAsHL7DTM(markerTime); //"20170612T104805.00"

      fhirobs << ",{\"reference\":\"#childObs" << ++obsId << "\"}";
      containedobs << ",{\"resourceType\":\"Observation\",\"id\":\"childObs"
                   << obsId << "\",\"meta\":{\"profile\":"
                   << "[\"http://hl7.org/fhir/uv/phd/StructureDefinition/"
                   << "PhdCodedEnumerationObservation\"]},\"status\":\"final\","
                   << "\"code\":{\"coding\":[{\"system\":"
                   << "\"urn:iso:std:iso:11073:10101\",\"code\":\"8450051\","
                   << "\"display\":\"MDC_CTG_PLACEHOLDER_EVENT\"}],\"text\":"
                   << "\"MDC_CTG_PLACEHOLDER_EVENT: User event\"},"
                   << "\"effectiveDateTime\":\"" << startTime << "\","
                   << "\"valueCodeableConcept\":{\"coding\":[{\"system\":"
                   << "\"urn:iso:std:iso:11073:10101\",\"code\":\"8450052\","
                   << "\"display\":\"MDC_CTG_PLACEHOLDER_EVENT_BUTTON\"}]}}";
    }
  
    // Insert the contained Observation
    fhirobs << "],\"contained\":[" << containedobs.str() << "]}\n";

    vector<string> retval(2);
    retval[0] = fhirobs.str();
    retval[1] = fhirdev.str();
    return retval;
  }

  /**********************************************************************/
  /*                                                                    */
  /*                   Monica communication protocol                    */
  /*                                                                    */
  /**********************************************************************/
  
  void MonicaCTG::Underpants::crcCalc(char x, uint16_t &crc) {
    crc ^= (((uint16_t)x) << 8);
    int i = 8;
    do {
      if (crc & 0x8000)
        crc = crc << 1 ^ 0x1021;
      else
        crc = crc << 1;
    } while(--i);
  }

  void MonicaCTG::Underpants::sendMsg(vector<char> &msg) {
    vector<char> output;
    uint16_t crc = 0;
    output.reserve(msg.size() + 6);
    output.push_back(0x10);
    crcCalc(0x10,crc);
    output.push_back(0x02);
    crcCalc(0x02,crc);
    for (char c : msg) {
      crcCalc(c,crc);
      output.push_back(c);
      if (c == 0x10) {
        crcCalc(c,crc);
        output.push_back(c);
      }  
    }
    output.push_back(0x10);
    crcCalc(0x10,crc);
    output.push_back(0x03);
    crcCalc(0x03,crc);
    output.push_back((crc >> 8) & 0xFF);
    output.push_back(crc & 0xFF);
  
    string m(output.data(),output.size());
    parent.write(sessionHandler, sessionHandle, m, true);
  }

  void MonicaCTG::Underpants::sendNote(string payload) {
    vector<char> msg;
    msg.reserve(payload.length()+5);
    msg.push_back('N');
    msg.push_back('0');
    msg.push_back('2');
    msg.push_back('P');
    msg.push_back('C');
    int i;
    for (i = 0; i<payload.length(); i++) {
      msg.push_back(payload[i]);
    }
    sendMsg(msg);
  }

  void MonicaCTG::Underpants::dispatchInputMsg(vector<char> &msg) {
    string s(msg.data(),msg.size());
    size_t i;
    //printf("Input %lu chars: '%s'\n", msg.size(), s.c_str());

    auto it = msg.cbegin();

    if (msg.size() == 0) return; // Just in case...
    switch (msg[0]) {
    case 'N': // 'Note' message
      // We reject packets not starting with "02AN" as invalid formatted
      // Note that the Monica violates the Philips protocol here, as the
      // header length should have been a single byte but is instead two
      // hex digits...
      if (msg.size() > 5 && msg[1]=='0' && msg[2]=='2' && msg[3]=='A'
          && msg[4]=='N') {
        handleNoteMsg(msg.cbegin()+5, msg.cend());
      }
      break;
    case 'I': // 'Info' message
      if (msg.size() != 27) break; // bad format

      // Fill out device details
      i = btName.find_first_of(" ");
      if (i != string::npos) {
        //"AN24 A001286"->"A001286"
        device.serialNo = btName.substr(i+1);
      }
      device.btMac.clear();
      for (char c : btMAC) {
        //"00:80:98:0E:39:13"->"0080980E3913"
        if (c != ':') device.btMac.push_back(c);
      }
      device.id = string(it + 1, it + 7);
      device.protocolRev = string({it[7], '.', '0', it[8], '.', '0', it[9]});
      device.swRev = string(it + 10, it + 17);
      device.fwRev = string({it[23], '.', it[24], it[25], it[26]});

    if ((state == GET_VERSION || state == GET_VERSION_WR) && lastResult == 0) {
        // Send "Patient status" message
        // Fixed antenatal and disable accelerometer
        lastResult++;
        if (state == GET_VERSION_WR) {
          state = GET_VERSION;
          cancelTimer();
          switch (observation.patientStatus) {
          case CTGObservation::ANTENATAL:
            sendNote("PST\001\0");
            break;
          case CTGObservation::INDUCTION:
            sendNote("PST\002\0");
            break;
          case CTGObservation::LABOUR:
            sendNote("PST\003\0");
            break;
          default: // UNKNOWN
            sendNote("PST\0\0");
          }
        } else {
          state = GET_VERSION_RD;
        }
      }
      break;
    case 'C': // 'CTG sample' message
      if (msg.size() != 34 || state != MONITORING) break; // bad format
      // We expect Status = 0x0000, HR Mode = 0x4160, Toco mode = 0x08
      if (it[1] != 0 || it[2] != 0 || it[31] != 0x41 || it[32] != 0x60 || it[33] != 0x08) {
        parent.warn("Sample dropped - unexpected mode data");
      } else {
        CTGSample *sample;
        int i;
        for (i = 0; i < 4; i++) {
          observation.samples.emplace_back();
          sample = &observation.samples.back();
          sample->fhr = ((uint16_t)it[3+i*2])<<8 | (it[4+i*2] & 0xFF);
          sample->mhr = ((uint16_t)it[19+i*2])<<8 | (it[20+i*2] & 0xFF);
          sample->toco = it[27+i];
        }
        switch ((it[25] >> 5) & 0x03) {
        case 2:
          reportQuality(GREEN);
          break;
        case 1:
          reportQuality(YELLOW);
          break;
        default:  
          reportQuality(RED);
        }
      }
      break;
    case 'M': // 'Marker button event' message
      if (msg.size() != 2 || it[1] != 'M' || state != MONITORING) break; // bad format
      observation.markers.push_back(observation.samples.size());
      reportEvent(BUTTON_PRESSED);
      parent.dbg("Marker button pressed");
      break;
    }
  
  }

  void MonicaCTG::Underpants::handleNoteMsg(vector<char>::const_iterator payload, vector<char>::const_iterator end) {
    if ((end-payload) == 9 && payload[0] == 'P') {
      if (string(payload+1, end) ==
          ((observation.patientStatus == CTGObservation::ANTENATAL) ?
           "00000001" : ((observation.patientStatus == CTGObservation::INDUCTION) ?
                        "00000002" :
                        ((observation.patientStatus == CTGObservation::LABOUR) ?
                         "00000003" : "00000000"))) && lastResult == 1 &&
          (state == GET_VERSION || state == GET_VERSION_WR)) {
        lastResult++;
        if (state == GET_VERSION_WR) {
          state = GET_VERSION;
          cancelTimer();
          sendNote("UAD");
        } else {
          state = GET_VERSION_RD;
        }
      }
    } else if ((end-payload) == 9 && payload[0] == 'U') {
      observation.uaDelay = std::stoul(string(payload+1, end),nullptr,16);
      if ((state == GET_VERSION || state == GET_VERSION_WR) && lastResult == 2) {
        lastResult++;
        if (state == GET_VERSION_WR) {
          state = IMPEDANCE;
          cancelTimer();
          reportConnState(CONNECTED);
          reportEvent(LEADCHECK_STARTED);
          parent.dbg("Testing leads...");
          sendNote("IM3");
        } else {
          state = GET_VERSION_RD;
        }
      }
    } else if ((end-payload) == 3 && payload[0] == 'i') {
      if ((state == IMPEDANCE) || state == IMPEDANCE_WR) {
        uint8_t is1 = payload[1];
        uint8_t is2 = payload[2];
        if (is1 == 0xF1 && is2 == 0) { // Impedance ok, start monitoring
          capFail = greenFail = whiteFail = orangeFail = yellowFail = false;
          reportLeadCheck(OK,OK,OK,OK,OK);
          if (state == IMPEDANCE_WR) {
            cancelTimer();
            reportEvent(LEADCHECK_STOPPED);
            state = MODE;
            sendNote("CONT");
          } else {
            state = IMPEDANCE_RD;
            lastResult = 0;
          }
          return;
        } else if (is1 != 0 || is2 != 0) { // not ok yet
          reportLeadCheck(leadStatusValue(is1 & 0x01, is2 & 0x01),
                          leadStatusValue(is1 & 0x02, is2 & 0x02),
                          leadStatusValue(is1 & 0x04, is2 & 0x04),
                          leadStatusValue(is1 & 0x08, is2 & 0x08),
                          leadStatusValue(is1 & 0x10, is2 & 0x10));
        }
        if (state == IMPEDANCE_WR) {
          state = IMPEDANCE;
          cancelTimer();
          sendNote("IM3");
        } else {
          state = IMPEDANCE_RD;
        }
      }
    } else if ((end-payload) == 9 && payload[0] == 'E' && payload[1] == 'N' && payload[2] == 'D') {
      if (state == MONITORING) {
        cancelTimer();
        reportEvent(RECORDING_STOPPED);
        reportConnState(DISCONNECTING);
        state = END_RECEIVED;
        sendNote("END");
      } else if (state == READY) {
        reportConnState(DISCONNECTING);
        state = END_PENDING;
      }
    } else if ((end-payload) == 9 && payload[0] == 'S' && state == MONITORING) {
      // SNR
      reportSNR(stoi(string(payload + 1, payload + 5), 0, 16),
                stoi(string(payload + 5, payload + 9), 0, 16));
    } else if ((end-payload) == 1 && state == MONITORING) {
      switch (payload[0]) {
      case 'B':
        // Battery low
        reportEvent(BATTERY_LOW);
        break;
      case 'C':
        capFail = true;
        resetFailTimer();
        break;
      case '1':
        greenFail = true;
        resetFailTimer();
        break;
      case '2':
        whiteFail = true;
        resetFailTimer();
        break;
      case '3':
        orangeFail = true;
        resetFailTimer();
        break;
      case '4':
        yellowFail = true;
        resetFailTimer();
        break;
      case 'L':
        capFail = greenFail = whiteFail = orangeFail= yellowFail= false;
        resetFailTimer();
        break;
      }
    }
  }

  void MonicaCTG::Underpants::resetFailTimer() {
    setTimer(LEADFAIL_DELAY, [this]() -> void {
        if (state == MONITORING) {
          reportLeadFail(capFail? BAD:OK, greenFail? BAD:OK, whiteFail? BAD:OK,
                         orangeFail? BAD:OK, yellowFail? BAD:OK);
        }
      });
  }

  /**********************************************************************/
  /*                                                                    */
  /*                          Timer functions                           */
  /*                                                                    */
  /**********************************************************************/

  void MonicaCTG::Underpants::setTimer(uint32_t delayMillis, function<void()> callback) {
    currentDelay = delayMillis;
    currentCallback = callback;
    switch (timerState) {
    case TIMER_IDLE:
      timerState = TIMER_STARTING;
      parent.startTimer(timer, delayMillis, false);
      break;
    case TIMER_IDLE_STOP_PENDING:
      timerState = TIMER_RESTART_PENDING;
      break;
    case TIMER_IDLE_START_PENDING:
    case TIMER_STARTING:
      timerState = TIMER_RESET_PENDING;
      break;
    case TIMER_RUNNING:
      break;
    default:
      break;
    }
  }

  void MonicaCTG::Underpants::cancelTimer() {
    switch (timerState) {
    case TIMER_RESET_PENDING:
    case TIMER_STARTING:
      timerState = TIMER_IDLE_START_PENDING;
      break;
    case TIMER_RUNNING:
      parent.stopTimer(timer,currentTimerId);
      // No break here!
    case TIMER_RESTART_PENDING:
      timerState = TIMER_IDLE_STOP_PENDING;
      break;
    default:
      break;
    }
  }

  /**********************************************************************/
  /*                                                                    */
  /*                          Helper functions                          */
  /*                                                                    */
  /**********************************************************************/

  void MonicaCTG::Underpants::searchTimeout() {
    if (searchActive && state == OPENING) {
      parent.searchStop(searchId);
      searchActive = false;
      state = NOT_CONNECTED;
      reportConnState(DISCONNECTED);
      parent.startRecordingFailed(master, CoreError("No Monica device found"));
    }
  }
  
  bool MonicaCTG::Underpants::stateOpenSession() {
    return state == OPEN || state == GET_VERSION ||
      state == GET_VERSION_WR || state == GET_VERSION_RD ||
      state == IMPEDANCE || state == IMPEDANCE_WR ||
      state == IMPEDANCE_RD || state == MODE || state == READY ||
      state == MONITORING;
  }

  void MonicaCTG::Underpants::sessionReport() {
    if (state != STOP_PENDING) reportConnState(DISCONNECTED);
    state = NOT_CONNECTED;
    if (observation.samples.size()>0) {
      vector<string> r = generateFHIR();
      parent.stopRecordingSuccess(master, r[0], r[1]);
    } else {
      parent.stopRecordingFailed(master, "No recording was made");
    }    
  }

  // typedef enum {DISCONNECTED, CONNECTING, CONNECTED, DISCONNECTING} connState;
  void MonicaCTG::Underpants::reportConnState(connState newState) {
    stringstream status;
    status << "{\"connectionState\":\"";
    switch (newState) {
    case CONNECTING:
      status << "CONNECTING\"}";
      break;
    case CONNECTED:
      status << "CONNECTED\",\"device\":{\"bd_addr\":\"" << btMAC
             << "\",\"manufacturer\":\"" << device.manufacturer
             << "\",\"modelNumber\":\"" << device.id
             << "\",\"serialNumber\":\"" << device.serialNo << "\"}}";
      break;
    case DISCONNECTING:
      status << "DISCONNECTING\"}";
      break;
    case DISCONNECTED:
      status << "DISCONNECTED\"}";
      break;
    default:
      return;
    }
    parent.statusChanged(master, status.str());
  }

  // typedef enum {LEADCHECK_STARTED, LEADCHECK_STOPPED, RECORDING_STARTED,
  //  RECORDING_STOPPED, BUTTON_PRESSED, BATTERY_LOW } eventType;
  void MonicaCTG::Underpants::reportEvent(eventType event) {
    string eventName;
    switch (event) {
    case LEADCHECK_STARTED:
      eventName = "leadcheckStarted";
      break;
    case LEADCHECK_STOPPED:
      eventName = "leadcheckStopped";
      break;
    case RECORDING_STARTED:
      eventName = "recordingStarted";
      break;
    case RECORDING_STOPPED:
      eventName = "recordingStopped";
      break;
    case BUTTON_PRESSED:
      eventName = "buttonPressed";
      break;
    case BATTERY_LOW:
      eventName = "batteryLow";
      break;
    default:
      return;
    }
    parent.statusChanged(master, "{\"" + eventName + "Event\": true}");
  }

  // typedef enum {OK, NOT_GOOD, BAD} leadStatus;
  void MonicaCTG::Underpants::reportLeadCheck(leadStatus green,
                                              leadStatus white,
                                              leadStatus orange,
                                              leadStatus yellow,
                                              leadStatus black) {
    stringstream status;
    status << "{\"leadcheckObservation\":{\"green\":" << leadStatusName(green)
           << ",\"white\":" << leadStatusName(white)
           << ",\"orange\":" << leadStatusName(orange)
           << ",\"yellow\":" << leadStatusName(yellow)
           << ",\"black\":" << leadStatusName(black) << "}}";
    parent.statusChanged(master, status.str());
  }

  void MonicaCTG::Underpants::reportLeadFail(leadStatus cap,
                                             leadStatus green,
                                             leadStatus white,
                                             leadStatus orange,
                                             leadStatus yellow) {
    stringstream status;
    status << "{\"leadfailObservation\":{\"green\":" << leadStatusName(green)
           << ",\"white\":" << leadStatusName(white)
           << ",\"orange\":" << leadStatusName(orange)
           << ",\"yellow\":" << leadStatusName(yellow)
           << ",\"cap\":" << leadStatusName(cap) << "}}";
    parent.statusChanged(master, status.str());
  }

  // typedef enum {RED, YELLOW, GREEN} quality;
  void MonicaCTG::Underpants::reportQuality(quality q) {
    parent.statusChanged(master, string("{\"qualityObservation\":\"") +
                         (q == GREEN ? "GREEN" :
                          (q ==YELLOW ? "YELLOW" : "RED")) + "\"}");
  }

  void MonicaCTG::Underpants::reportSNR(uint16_t signal, uint16_t noise) {
    stringstream status;
    status << "{\"snrObservation\":{\"signal\":" << signal << ",\"noise\":"
           << noise << "}}";
    parent.statusChanged(master, status.str());
  }

  /**********************************************************************/
  /*                                                                    */
  /*                      MonicaCTG implementation                      */
  /*                                                                    */
  /**********************************************************************/

  //
  // Constructor / destructor
  //
  MonicaCTG::MonicaCTG(Context &context) : underpants(new Underpants(*this)), ModuleBase(context, "10000000") {
    start();
  }
  
  MonicaCTG::~MonicaCTG() {delete underpants;}

  //
  // Forward to underpants
  //
  void MonicaCTG::startRecording(peer_t peer, vector<pair<uint32_t, uint32_t>> properties) {
    underpants->startRecording(peer, properties);
  }
  
  void MonicaCTG::stopRecording(peer_t peer) {
    underpants->stopRecording(peer);
  }
  
  void MonicaCTG::openSuccess() {
    underpants->openSuccess();
  }

  void MonicaCTG::openFailed(CoreError error) {
    underpants->openFailed(error);
  }

  void MonicaCTG::closed(peer_t peer, objId_t port) {
    underpants->closed(peer, port);
  }
  
  void MonicaCTG::writeSuccess() {
    underpants->writeSuccess();
  }
  
  void MonicaCTG::writeFailed(CoreError error) {
    underpants->writeFailed(error);
  }
  
  void MonicaCTG::read(string input) {
    underpants->read(input);
  }

  void MonicaCTG::searchResult(objId_t search_id, DeviceTransport device) {
    underpants->searchResult(search_id,device);
  }

  void MonicaCTG::announceClock(BasePlate::peer_t peer) {
    underpants->announceClock(peer);
  }

  void MonicaCTG::readTimeSuccess(bool utc, std::string iso8601ext) {
    underpants->readTimeSuccess(utc,iso8601ext);
  }
  
  void MonicaCTG::readTimeSuccess(bool utc, ReadTimeSuccess_TimeParts parts) {
    underpants->readTimeSuccess(utc,parts);
  }
  
  void MonicaCTG::readTimeFailed(BasePlate::CoreError error) {
    underpants->readTimeFailed(error);
  }
  void MonicaCTG::startTimerSuccess(int32_t timerId, bool fired) {
    underpants->startTimerSuccess(timerId, fired);
  }
  
  void MonicaCTG::startTimerFailed(BasePlate::CoreError error) {
    underpants->startTimerFailed(error);
  }
  
  void MonicaCTG::stopTimerSuccess() {
    underpants->stopTimerSuccess();
  }
  
  void MonicaCTG::stopTimerFailed(BasePlate::CoreError error) {
    underpants->stopTimerFailed(error);
  }
}} // Namespace