/*
 *   Copyright 2018 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */


/**
 * @file
 * @brief TEMPORARY SerialPort interface definition.
 *
 * This temporary hand-made header declares the interface of a serial
 * port. The intention is to replace all interface boiler plate by
 * auto-generated code in the near future.
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2018 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#ifndef SERIALPORT_INTERFACE_HPP
#define SERIALPORT_INTERFACE_HPP

#include "BasePlate.hpp"
#include <string>
#include "CTGControl_interface.hpp"
#include "Device.pb.h"
#include "Time.pb.h"

/**
 * @defgroup serialpal Serial port PAL interface
 * @ingroup group_pal_interfaces
 * @brief PoC serial port interface.
 * 
 * This interface is supposed to handle serial port communication
 * across the PAL. Currently, it is tightly bound to the Monica module
 * -- this should be fixed.
 *
 * This is a hand-generated interface declaration. It should be
 * replaced by an auto-generated definition in the near future.
 */

namespace s4 {
  namespace BasePlate {
    typedef std::string CoreError;
    typedef uint64_t objId_t;
  }
}

// FIXME: The namespace is a mess. This is a PAL interface!
namespace s4 { namespace monica_ctg
{
  /**
   * @brief The serial port interface.
   * @ingroup serialpal
   *
   * This is a hand-generated interface declaration. It should be
   * replaced by an auto-generated definition in the near future.
   */
  class SerialPortConnector {
  protected:
    virtual void open(BasePlate::peer_t peer, BasePlate::objId_t port) =0;
    virtual void openSuccess() =0;
    virtual void openFailed(BasePlate::CoreError error) =0;
    virtual void close(BasePlate::peer_t peer, BasePlate::objId_t port) =0;
    virtual void closed(BasePlate::peer_t peer, BasePlate::objId_t port) =0;
    virtual void write(BasePlate::peer_t peer, BasePlate::objId_t port, std::string msg, bool flush) =0;
    virtual void writeSuccess() =0;
    virtual void writeFailed(BasePlate::CoreError error) =0;
    virtual void read(std::string msg) =0;
    virtual void searchStart(BasePlate::objId_t search_id) =0;
    virtual void searchStop(BasePlate::objId_t search_id) =0;
    virtual void setStandardPasskey(BasePlate::objId_t handle, std::string passkey) =0;
    virtual void searchResult(BasePlate::objId_t search_id, ::s4::messages::classes::device::DeviceTransport device) =0;
    virtual void solicitClock() =0;
    virtual void announceClock(BasePlate::peer_t peer) =0;
    virtual void readTime(BasePlate::peer_t peer, bool utc, s4::messages::interfaces::time::ReadTime_Format format) =0;
    virtual void readTimeSuccess(bool utc, std::string iso8601ext) =0;
    virtual void readTimeSuccess(bool utc, s4::messages::interfaces::time::ReadTimeSuccess_TimeParts parts) =0;
    virtual void readTimeFailed(BasePlate::CoreError error) =0;
    virtual void startTimer(BasePlate::peer_t peer, uint32_t delayMillis, bool repeat) =0;
    virtual void startTimerSuccess(int32_t timerId, bool fired) =0;
    virtual void startTimerFailed(BasePlate::CoreError error) =0;
    virtual void stopTimer(BasePlate::peer_t peer, int32_t timerId) =0;
    virtual void stopTimerSuccess() =0;
    virtual void stopTimerFailed(BasePlate::CoreError error) =0;
  };

  // We only use the Plug-end of the interface
  /**
   * @brief The plug implementation of the serial port interface
   * @ingroup serialpal
   */
  class SerialPortPlugC : protected SerialPortConnector, virtual public BasePlate::ModuleBase {

  protected:
    SerialPortPlugC();
    void open(BasePlate::peer_t peer, BasePlate::objId_t port);
    void close(BasePlate::peer_t peer, BasePlate::objId_t port);
    void write(BasePlate::peer_t peer, BasePlate::objId_t port, std::string msg, bool flush);
    void searchStart(BasePlate::objId_t search_id);
    void searchStop(BasePlate::objId_t search_id);
    void setStandardPasskey(BasePlate::objId_t handle, std::string passkey);
    void solicitClock();
    void startTimer(BasePlate::peer_t peer, uint32_t delayMillis, bool repeat);
    void stopTimer(BasePlate::peer_t peer, int32_t timerId);
    void readTime(BasePlate::peer_t peer, bool utc, s4::messages::interfaces::time::ReadTime_Format format);
  };
}} // Namespace
#endif // SERIALPORT_INTERFACE_HPP
