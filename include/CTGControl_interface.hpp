/*
 *   Copyright 2018 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */


/**
 * @file
 * @brief TEMPORARY CTGControl interface definition.
 *
 * This temporary hand-made header declares the interface of a CTG
 * recorder. The intention is to replace all interface boiler plate by
 * auto-generated code in the near future.
 *
 * Furthermore, the control interface mixes device control and the
 * observation reporting (FHIR bundle). These interfaces should be
 * separated as they are at two different abstraction levels. In fact,
 * the device control logically sits at the same level as the
 * (yet-to-be-defined) IEEE DIM interface - it may actually be
 * implemented as actions on a DIM Scanner object.
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2018 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#ifndef CTGCONTROL_INTERFACE_HPP
#define CTGCONTROL_INTERFACE_HPP

#include "BasePlate.hpp"
#include <string>
#include <utility>
#include <vector>

/**
 * @defgroup ctgcontrolinterface CTG control interface
 * @ingroup group_core_interfaces
 * @brief PoC CTG control interface.
 * 
 * This interface is supposed to handle the control of a CTG recording
 * (starting and stopping). Currently, it is tightly bound to the
 * Monica module and to the conversion into FHIR -- this should be
 * fixed.
 *
 * This is a hand-generated interface declaration. It should be
 * replaced by an auto-generated definition in the near future.
 */

namespace s4 {
  namespace BasePlate {
    typedef std::string CoreError;
    typedef uint64_t objId_t;
  }
}
  
namespace s4 { namespace monica_ctg {
  /**
   * @brief The CTG control interface.
   * @ingroup ctgcontrolinterface
   *
   * This is a hand-generated interface declaration. It should be
   * replaced by an auto-generated definition in the near future.
   */
  class CTGControlConnector {
  protected:
    virtual void startRecording(BasePlate::peer_t peer, std::vector<std::pair<uint32_t,uint32_t>> properties) =0;
    virtual void startRecordingSuccess(BasePlate::peer_t peer) =0;
    virtual void startRecordingFailed(BasePlate::peer_t peer, BasePlate::CoreError err) =0;
    virtual void stopRecording(BasePlate::peer_t peer) =0;
    virtual void stopRecordingSuccess(BasePlate::peer_t peer, std::string fhirObservation, std::string fhirDevice) =0;
    virtual void stopRecordingFailed(BasePlate::peer_t peer, BasePlate::CoreError err) =0;
    virtual void statusChanged(BasePlate::peer_t peer, std::string statusText) =0;
  };

  // We only use the Outlet-end of the interface
  class CTGControlOutlet : protected CTGControlConnector, virtual public BasePlate::ModuleBase {
  protected:
    CTGControlOutlet();
    void startRecordingSuccess(BasePlate::peer_t peer);
    void startRecordingFailed(BasePlate::peer_t peer, BasePlate::CoreError err);
    void stopRecordingSuccess(BasePlate::peer_t peer, std::string fhirObservation, std::string fhirDevice);
    void stopRecordingFailed(BasePlate::peer_t peer, BasePlate::CoreError err);
    void statusChanged(BasePlate::peer_t peer, std::string statusText);
  };
}} // Namespace
#endif // CTGCONTROL_INTERFACE_HPP
