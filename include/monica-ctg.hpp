/*
 *   Copyright 2018 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Monica AN24 communication module.
 *
 * This module is a proof-of-concept implementation of a communication
 * module capable of performing a CTG recording on a Monica CTG device
 * and outputting a FHIR bundle containing the result.
 *
 * This module should be split into several modules, one translating
 * from the CTG device to the IEEE domain information model and one
 * for translating generic DIM objects to a FHIR bundle and/or FHIR
 * server upload.
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2018 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#ifndef MONICA_CTG_HPP
#define MONICA_CTG_HPP

#include "BasePlate.hpp"
#include "SerialPort_interface.hpp"
#include "CTGControl_interface.hpp"

/**
 * @defgroup monicactg Monica AN24 communication module
 * @ingroup group_core
 * @brief PoC implementation of Monica AN24 communication.
 * 
 * This module is capable of performing a CTG recording on a Monica
 * CTG device, outputting a FHIR bundle containing the result.
 *
 * It should be split into several modules, one translating from the
 * CTG device to the IEEE domain information model and one for
 * translating generic DIM objects to a FHIR bundle and/or FHIR server
 * upload.
 */
namespace s4 { namespace monica_ctg {

  /**
   * @ingroup monicactg
   * @brief Monica AN24 communication module
   *
   * This module is a proof-of-concept implementation of a communication
   * module capable of performing a CTG recording on a Monica CTG device
   * and outputting a FHIR bundle containing the result.
   *
   * This module should be split into several modules, one translating
   * from the CTG device to the IEEE domain information model and one
   * for translating generic DIM objects to a FHIR bundle and/or FHIR
   * server upload.
   */
  class MonicaCTG final : protected CTGControlOutlet, public SerialPortPlugC {
    
  public:
    MonicaCTG(BasePlate::Context &context);
    ~MonicaCTG();
    
  private:
    // CTGControlOutlet implementation
    
    void startRecording(BasePlate::peer_t peer, std::vector<std::pair<uint32_t, uint32_t>> properties);
    void stopRecording(BasePlate::peer_t peer);

    // SerialPortPlug implementation
    
    void openSuccess();
    void openFailed(BasePlate::CoreError error);
    void closed(BasePlate::peer_t peer, BasePlate::objId_t port);
    void writeSuccess();
    void writeFailed(BasePlate::CoreError error);
    void read(std::string input);
    void searchResult(BasePlate::objId_t search_id, ::s4::messages::classes::device::DeviceTransport device);
    
    void announceClock(BasePlate::peer_t peer);
    void readTimeSuccess(bool utc, std::string iso8601ext);
    void readTimeSuccess(bool utc, s4::messages::interfaces::time::ReadTimeSuccess_TimeParts parts);
    void readTimeFailed(BasePlate::CoreError error);
    void startTimerSuccess(int32_t timerId, bool fired);
    void startTimerFailed(BasePlate::CoreError error);
    void stopTimerSuccess();
    void stopTimerFailed(BasePlate::CoreError error);

    /// @brief Opaque wrapper of the private parts.
    class Underpants;
    friend class Underpants;
    
    /// @brief Container of the private parts.
    Underpants *underpants;
  };
}} // Namespace

#endif // MONICA_CTG_HPP